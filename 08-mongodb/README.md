# 8. sedmica vežbi

## Node.js + MongoDB/Mongoose - Kreiranje serverskih aplikacija sa podrškom za SUBP

- [Primeri sa časa](./primeri/)
- [MongoDB tekst](./tekst_za_skriptu/mongodb.md)
    - Ovo je preliminarna verzija teksta (od prošle godine). Kada budemo ažurirali skriptu, tada ćemo ažurirati i ovaj link.
    - Neki primeri iz teksta se malo razlikuju u odnosu na one koje smo radili na času.
    - Posebno obratiti pažnju na šifrovanje lozinki i paket `bcrypt`.

