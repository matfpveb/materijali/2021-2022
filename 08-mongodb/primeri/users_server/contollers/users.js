// U zavisnosti od toga koji users servis zelite da koristite,
// treba ukljuciti odgovarajuci modul. Nemaju svi korisnici sifrovane lozinke,
// pa kada budete testirali npr. changeUserPassword iz users_with_passwords,
// treba testirati nad korisnicima koji imaju sifrovane lozinke (ili napraviti nove).

// Bez sifrovanja:
// const usersService = require('../services/users')
// Sa sifrovanjem:
const usersService = require('../services/users_with_passwords');

// Sledeci modul se koristi za validacije mejlova, imena i slicno (pogledajte ispod koriscenje). 
// Kasnije cemo ovo raditi na sofisticiranje nacine.
// Mozete umesto ovoga koristiti i regularne izraze.
const validator = require('validator');

const getAllUsers = async (req, res, next) => {
  try {
    const allUsers = await usersService.getAllUsers();
    res.status(200).json(allUsers);
  } catch (error) {
    next(error);
  }
};

const getUserByUsername = async (req, res, next) => {
  const username = req.params.username;

  try {
    if (username == undefined) {
      const error = new Error('Nedostaje korisnicko ime!');
      error.status = 400;
      throw error;
    }

    const user = await usersService.getUserByUsername(username);
    if (user == null) {
      res.status(404).json();
    } else {
      res.status(200).json(user);
    }
  } catch (error) {
    next(error);
  }
};

const getUsersByStatus = async (req, res, next) => {
  const status = req.params.status;

  try {
    if (status == undefined) {
      const error = new Error('Nedostaje status!');
      error.status = 400;
      throw error;
    }

    const user = await usersService.getUsersByStatus(status);
    if (user == null) {
      res.status(404).json();
    } else {
      res.status(200).json(user);
    }
  } catch (error) {
    next(error);
  }
};

const addNewUser = async (req, res, next) => {
  const { username, email, password, age, programmingSkills, status } = req.body;

  try {
    if (
      !username ||
      !email ||
      !password ||
      !validator.isEmail(email) ||
      !validator.isAlphanumeric(username)
    ) {
      const error = new Error('Proverite prosledjene podatke!');
      error.status = 400;
      throw error;
    }

    const user = await usersService.getUserByUsername(username);
    if (user) {
      const error = new Error('Ovo korisnicko ime je zauzeto!');
      error.status = 403;
      throw error;
    }

    const newUser = await usersService.addNewUser(
      username,
      email,
      password,
      age,
      programmingSkills, 
      status
    );
    res.status(201).json(newUser);
  } catch (error) {
    next(error);
  }
};

const changeUserPassword = async (req, res, next) => {
  const { username, oldPassword, newPassword } = req.body;

  try {
    if (!username || !oldPassword || !newPassword) {
      const error = new Error('Proverite prosledjene podatke!');
      error.status = 400;
      throw error;
    }

    const user = await usersService.getUserByUsername(username);

    if (!user) {
      const error = new Error('Proverite korisnicko ime i/ili sifre!');
      error.status = 404;
      throw error;
    }

    const updatedUser = await usersService.changeUserPassword(username, oldPassword, newPassword);

    if (updatedUser) {
      res.status(200).json(updatedUser);
    } else {
      const error = new Error('Proverite korisnicko ime i/ili sifre!');
      error.status = 403;
      throw error;
    }
  } catch (error) {
    next(error);
  }
};

const deleteUser = async (req, res, next) => {
  const username = req.params.username;

  try {
    if (!username) {
      const error = new Error('Nedostaje korisnicko ime!');
      error.status = 400;
      throw error;
    }

    const user = await usersService.getUserByUsername(username);
    if (!user) {
      const error = new Error('Proverite korisnicko ime!');
      error.status = 404;
      throw error;
    }

    await usersService.deleteUser(username);
    res.status(200).json({success: true});
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getUserByUsername,
  getAllUsers,
  addNewUser,
  changeUserPassword,
  deleteUser,
};
