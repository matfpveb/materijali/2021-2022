const jwt = require('jsonwebtoken');

const jwtSecret = process.env.JWT_SECRET || 'pveb_showtime';
const adminUsername = process.env.ADMIN_USERNAME || 'admin';
const adminPassword = process.env.ADMIN_PASSWORD || 'adminpass';
const jwtOpts = { algorithm: 'HS256', expiresIn: '30d' };

// token koji treba proslediti je (ne smete proslediti i novi red sa kraja!):
/*
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pblVzZXJuYW1lIjoiYWRtaW4iLCJhZG1pblBhc3N3b3JkIjoiYWRtaW5wYXNzIiwiaWF0IjoxNjM4MTk4OTAxLCJleHAiOjE2NDA3OTA5MDF9.DavOyF3ST-3Ynu2bBm7NhAXaAESBJ4KV5sFCf2P8aCI
*/

/*
  Detaljnije objasnjenje:

  Ovaj token iznad smo dobili pozivanjem metode jwt.sign(data, jwtSecret, jwtOps). 
  'Data' je jedan jednostavan objekat u kome se nalaze svojstva adminUsername i adminPassword,
  sa vrednostima koje smo iznad inicijalizovali. Medjutim, token ce biti razlicit za svakog korisnika.
  To je i logicno - nema smisla koristiti isti token za vise korisnika. Nakon sto je token kreiran, on se
  salje klijentu i tamo se i cuva! Znaci server nece pamtiti nista sem svoje tajne (jwtSecret). Njemu
  ce klijent svaki put slati svoj token na proveru. Na osnovu nase tajne, i tokena koji je klijent poslao,
  mi cemo pozivati metod jwt.verity(), kao sto to radimo ispod.

  Kako bismo demonstrirali to bez pravljenja kompleksne aplikacije, kreiramo jedan zajednicki token koji ce
  svi koristiti. Dakle, u ovoj nasoj aplikaciji, svi ce imati token namenjen adminu! 
  
  Medjutim, primetimo takodje i sledecu stvar.
  Ovde je kao opcija podeseno da ovaj token istice nakon 30 dana. Iz tog razloga, moramo svakih 30 dana kreirati
  novi token, inace on nece biti validan. U slucaju da je proslo 30 dana od postavljanja ovog koda, novi token
  mozete generisati na sledeci nacin (da ovo uradite, samo pokrenite fajl generate_token.js iz tekuceg direktorijuma):

  const data = {
    adminUsername,
    adminPassword
  }
  const token = jwt.sign(data, jwtSecret, jwtOpts);
  console.log(token);
  
  Nakon sto ga ispisite u konzolu, sacuvajte taj token negde pod komentarom radi lakseg koriscenja. Onda, svaki put kada
  budete slali zahtev koji zahteva verifikaciju (pogledati u routes/api/users.js), kao zaglavlje zahteva podesicete
  i zaglavlje 'x-access-token', kao vrednost tog zaglavlja postavljate vas novokreirani token (ili ovaj odavde ako je
  i dalje validan), a telo zahteva postavljate kao i do sad. Napominjem da ce se ovo raditi u dva razlicita Postman taba,
  ali ce to sve biti jedan zahtev na kraju. Header postavljate u tabu 'Headers', gde ce key biti 'x-access-token',
  a value vrednost tokena. U tabu Body postavljate sve stvari koje zahtev sadrzi u telu.
*/

const verifyToken = (req, res, next) => {
  let token = req.headers['x-access-token'];

  if (!token) {
    return res.status(403).json({ auth: false, message: 'Nije prosledjen token.' });
  }

  jwt.verify(token, jwtSecret, (error, adminData) => {
    if (error) {
      return res
        .status(500)
        .json({ auth: false, message: 'Doslo je do problema prilikom provere tokena.' });
    }
    if (
      adminData.adminUsername != adminUsername ||
      adminData.adminPassword != adminPassword
    ) {
      return res.status(403).json({auth: false, message: 'Pogresno korisnicko ime i/ili sifra'});
    }
    
    next();
  });
};

module.exports = verifyToken;
