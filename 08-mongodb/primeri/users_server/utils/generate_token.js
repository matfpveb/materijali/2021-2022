const jwt = require('jsonwebtoken');

const jwtSecret = process.env.JWT_SECRET || 'pveb_showtime';
const adminUsername = process.env.ADMIN_USERNAME || 'admin';
const adminPassword = process.env.ADMIN_PASSWORD || 'adminpass';
const jwtOpts = { algorithm: 'HS256', expiresIn: '30d' };

const data = {
    adminUsername,
    adminPassword
}

const token = jwt.sign(data, jwtSecret, jwtOpts);
console.log(token);