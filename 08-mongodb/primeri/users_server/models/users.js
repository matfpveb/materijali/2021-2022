const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    password: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    email: {
        type: mongoose.Schema.Types.String,
        required: true  
    },
    status: {
        type: mongoose.Schema.Types.String,
        default: "active"
    }, 
    age: mongoose.Schema.Types.Number,
    programmingSkills: [mongoose.Schema.Types.String]
});

const User = mongoose.model('User', userSchema);

module.exports = User;
