const mongoose = require('mongoose');

const databaseString =
  process.env.DB_STING || 'mongodb://localhost:27017/users';

mongoose.connect(databaseString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.once('open', function () {
  console.log('Uspesno povezivanje!');
});

mongoose.connection.on('error', (error) => {
  console.log('Greska: ', error);
});

const userSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  username: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  password: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  email: {
    type: mongoose.Schema.Types.String,
    required: true,
  },
  status: {
    type: mongoose.Schema.Types.String,
    default: 'active',
  },
  age: mongoose.Schema.Types.Number,
  programmingSkills: [mongoose.Schema.Types.String]
});

const User = mongoose.model('User', userSchema);