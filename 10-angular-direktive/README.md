# 10. sedmica vežbi

## Angular - Direktive i komponente (napredno)

- [Sekcije 8.5 i 8.6 iz skripte](http://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf)
- [Prezentacija sa časa](./Angular-2.pdf)
- [Aplikacija sa časa](./store-spa/). Izgled aplikacije:

![Store SPA](./app-images/store-spa2-1.png)
![Store SPA](./app-images/store-spa2-2.png)
![Store SPA](./app-images/store-spa2-3.png)
![Store SPA](./app-images/store-spa2-4.png)
![Store SPA](./app-images/store-spa2-5.png)
![Store SPA](./app-images/store-spa2-6.png)
