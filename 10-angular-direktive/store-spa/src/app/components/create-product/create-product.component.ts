import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Product } from 'src/app/models/product.model';

// Problem koji smo imali na casu dolazi iz toga sto smo EventEmitter importovali iz modula 'stream',
// a ne iz '@angular/core' kao sto je trebalo. To je EventEmitter klasa koju koristimo ovde.

declare const $: any;

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  @ViewChild('inputName', {static: false}) inputName: ElementRef = {} as ElementRef;
  @ViewChild('inputPrice', {static: false}) inputPrice: ElementRef = {} as ElementRef;
  @ViewChild('inputDescription', {static: false}) inputDescription: ElementRef = {} as ElementRef;
  @ViewChild('inputForSale', {static: false}) inputForSale: ElementRef = {} as ElementRef;

  @Output() productCreated: EventEmitter<Product> = new EventEmitter<Product>();

  constructor() { }

  ngOnInit(): void {
    $('.ui.checkbox').checkbox();
  }

  addNewProduct() {
    const name: string = (this.inputName.nativeElement as HTMLInputElement).value;
    const price: number = Number.parseFloat((this.inputPrice.nativeElement as HTMLInputElement).value);
    const description: string = (this.inputDescription.nativeElement as HTMLInputElement).value;
    const forSale: boolean = (this.inputForSale.nativeElement as HTMLInputElement).checked;

    // kratka validacija
    if(name === '' || Number.isNaN(price) || description === '') {
      window.alert("Some required fields are missing");
      return;
    }

    const newProduct: Product = new Product(name, description, price, forSale, '/assets/default-product.jpg', 'Pera Peric', 90);
    
    this.productCreated.emit(newProduct);
  }

}
