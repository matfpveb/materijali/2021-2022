import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  
  user: User;
  shouldDisplayUserForm: boolean = false; 

  constructor() { 
    this.user = new User("Pera Peric", "laza@gmail.com", "/assets/default-user.png");
  }

  ngOnInit(): void {
  }

  onChangeInfo() {
    this.shouldDisplayUserForm = true;
  }

  onSaveChanges() {
    this.shouldDisplayUserForm = false;
  }

  onChangeName(event: Event) {
    const name = (event.target as HTMLInputElement).value;
    this.user.name = name;
  }

  onChangeEmail(event: Event) {
    const email = (event.target as HTMLInputElement).value;
    this.user.email = email;
  }

  getColor(paranIndex: boolean): string {
    return paranIndex ? "black" : "gray";
  }

}
