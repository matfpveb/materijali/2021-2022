# 2021-2022

Ovaj repozitorijum sadrži zvanične materijale sa časova vežbi tokom akademske 2021/22. godine.

# Snimci sa vežbi

|Nedelja | Link |
|:--------|:------|
| 1 | Uvod u JavaScript |
| 2 | [Nizovi i objekti](https://matf.webex.com/matf/ldr.php?RCID=0b46d391b9c3c2b4ed1bd03c1270d4ee)|
| 3 | [Objektno-orijentisana paradigma](https://matf.webex.com/matf/ldr.php?RCID=4a7d61f83c109ac5b6a85475f83cc564)|
| 4 | [Rad sa greškama, moduli, asinhrona paradigma - prvi deo ](https://matf.webex.com/matf/ldr.php?RCID=6a96cd0ed00d90edea996ea448254c2b)|
| 5 | [Asinhrona paradigma - drugi deo, TypeScript](https://matf.webex.com/matf/ldr.php?RCID=c059dd8894d5e1ed30daff2e5e5cdbfd)
| 6 | [Uvod u Node.js, ugrađeni Node moduli](https://matf.webex.com/matf/ldr.php?RCID=16fd1e8c1c2c4be4d98983aec9d9043d)
| 7 | [Uvod u Express.js, serverska aplikacija za rad sa korisnicima](https://matf.webex.com/matf/ldr.php?RCID=67abf974ced91265f78e7e11f17daafd)
| 8 | [Uvod u MongoDB, Mongoose ODM, nadogradnja serverske aplikacije sa prošlog časa](https://matf.webex.com/matf/ldr.php?RCID=9ae34457296c0197119052cccfa12b84)
| 9 | [Uvod u Angular, Angular komponente, vezivanje podataka](https://matf.webex.com/matf/ldr.php?RCID=ec361a071e89a48a04cb5a9049f93b03)
| 10 | [Angular direktive, tok podataka između komponenti](https://matf.webex.com/matf/ldr.php?RCID=b489b869c43f11573789d3b85432f18c)
| 11 | [Angular reaktivni formulari](https://matf.webex.com/matf/ldr.php?RCID=0876200304b99f5b3e88bcae53c52369)
| 12 | [Angular servisi, pohranjivanje datoteka](https://matf.webex.com/matf/ldr.php?RCID=5afcbe86a0bd397e7dc73c643bcd7475)
| 13 | [Angular rutiranje, filteri](https://matf.webex.com/matf/ldr.php?RCID=f82598cc0519bbcd880edbf2df779b76)
