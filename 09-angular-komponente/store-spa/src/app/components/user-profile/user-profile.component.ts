import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  
  user: User;
  
  changeUserField: string = "disabled field";

  constructor() { 
    this.user = new User("Pera Peric", "laza@gmail.com", "/assets/default-user.png");
  }

  ngOnInit(): void {
  }

  onChangeInfo() {
    this.changeUserField = "field";
  }

  onSaveChanges() {
    this.changeUserField = "disabled field";
  }

  onChangeName(event: Event) {
    const name = (event.target as HTMLInputElement).value;
    this.user.name = name;
  }

  onChangeEmail(event: Event) {
    const email = (event.target as HTMLInputElement).value;
    this.user.email = email;
  }

}
