# 9. sedmica vežbi

## Angular - Uvod i komponente

- [Sekcije 8.1, 8.2, 8.3 i 8.4 iz skripte](http://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf)
- [Prezentacija sa časa](./Angular-1.pdf)
- [Aplikacija sa časa](./store-spa/). Izgled aplikacije:

![Store SPA](./app-images/store-spa1.png)

## Korisne veze

- [Zvanična stranica Angular razvojnog alata](https://angular.io/){:target="_blank"}
- [Biblioteka Bootstrap](https://getbootstrap.com/){:target="_blank"}
- [Spisak interfejsa u Web API-u](https://developer.mozilla.org/en-US/docs/Web/API#Interfaces){:target="_blank"}

## Razne druge reference

- [Zvanična stranica AngularJS (Angular 1) razvojnog alata](https://angularjs.org/){:target="_blank"}
- [Stilizovanje dugmića pomoću Bootstrap biblioteke](https://getbootstrap.com/docs/4.4/components/buttons/){:target="_blank"}
- [Atributi `HtmlInputElement` interfejsa](https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement#Properties){:target="_blank"}
- [Događaji `HtmlInputElement` interfejsa](https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement#Events){:target="_blank"}
- [Interfejs `Event`](https://developer.mozilla.org/en-US/docs/Web/API/Event){:target="_blank"}