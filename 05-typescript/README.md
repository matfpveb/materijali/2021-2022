# 5. sedmica vežbi

## TypeScript

- Poglavlje 3 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}

### Zadaci za proveru znanja

- [Zadaci i pitanja](./zadaci_za_vezbu.md){:target="_blank"}
