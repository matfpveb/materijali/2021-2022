# Zadaci za vežbu

1. Transformisati kod
```javascript
function sum(a, b){
    return a + b;
}
```
dodavanjem TypeScript tipova tako da funkcija računa zbir prosleđenih brojeva. Da li će se prilikom transpiliranja prijaviti greška ako se kao jedan od argumenata prosledi vrednost `undefined`?

2. Transformisati kod
```javascript
function sumconcat(a, b){
    return a + b;
}
```
dodavanjem TypeScript tipova tako da funkcija ili računa zbir prosleđenih brojeva ili nadovezuje prosleđene niske. 

3. Napisati TypeScript funkciju `print` koja očekuje nisku `message` i opcioni parametar `error`. Ukoliko je naveden parametar `error` poruku treba ispisati velikim slovima. Funkcija nema povratnu vrednosti.

4. Kako se ponašaju TypeScript funkcije ukoliko se navede više argumenata nego što je predviđeno deklaracijom funkcije? Da li se prijavljuje greška u toku transpiliranja?

5. Čemu služi zastavica `-w` (ili u proširenoj formi `--watch`) koja se može navesti kao opcija `tsc` transpilatora? 

6. Napraviti TypeScript niz koji sadrži niske, a zatim napisati funkciju koja u zadatom nizu prebrojava koliko niski sadrži podnisku 'JS' (case insensitive). Na primer, za niz `['app.js', 'style.css', 'app.ts', 'script.JS', 'index.html']`, funkcija treba da vrati vrednost `2`. 

7. Napisati šablonsku funkciju koja zamenjuje vrednosti prvom i poslednjem elementu niza. Primeniti potom ovu funkciju nad nizom brojeva i nizom niski. 

8. Osmisliti TypeScript klasu koja omogućava rad sa `ToDo` listom. Zadatke predstaviti pomoću zasebne klase sa svojstvima `task`, `description` i `isDone`, a potom na nivou liste implementirati interfejs sa funkcijama `addNewTask`, `markTaskAsDone`, `deleteAllDone` i `countLeftTasks`. 

9. Napisati dekorator klase `@debugMode` koji proizvoljnoj klasi dodaje svojstvo `debugMode` sa vrednošću `true`.

10. Napisati TypeScript klasu koja kao svojstvo ima niz brojeva `arr` i metode `squareElements` i `sumElements` kojima se, redom, kvadriraju svi elementi niza i novi niz vraća kao rezultat, odnosno sabiraju svi elementi niza i zbir elemenata niza vraća kao rezultat. Napisati potom dekorator `@time` koji se može pridružiti pomenutim metodama i koji izračunava koliko vremena je potrebno da se opisane radnje izvrše. Za izračunavanje vremena izvršavanja metoda mogu se koristiti `console.time()` i `console.timeEnd()` pozivi.