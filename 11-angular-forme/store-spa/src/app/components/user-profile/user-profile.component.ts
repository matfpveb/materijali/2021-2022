import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  
  user: User;
  userForm: FormGroup;
  shouldDisplayUserForm: boolean = false; 

  constructor(private formBuilder: FormBuilder) { 
    this.user = new User('peraperic', 'Pera Peric', 'pera@gmail.com', '/assets/default-user.png');

    this.userForm = new FormGroup({
      username: new FormControl(this.user.username, [Validators.required, Validators.pattern(new RegExp("[a-zA-Z0-9_-]{8,}"))]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email]),
      name: new FormControl(this.user.name, [Validators.required, Validators.minLength(2)]),
      imgUrl: new FormControl('')
    });
  }

  ngOnInit(): void {
  
  }

  onChangeInfo() {
    this.shouldDisplayUserForm = true;
  }

  onSaveChanges() {
    this.shouldDisplayUserForm = false;
  }

  onUserFormSubmit() {
    const data = this.userForm.value;
    
    if (this.userForm.invalid) {
      window.alert('Form is not valid!');
      return;
    } 

    this.user.username = data.username;
    this.user.name = data.name;
    this.user.email = data.email;

    this.userForm.reset({
      username: this.user.username,
      name: this.user.name,
      email: this.user.email,
    });

    this.onSaveChanges();
  }

}
