# 11. sedmica vežbi

## Angular - Reaktivni formulari, servisi i HttpClient

- [Sekcije 8.8, 8.11 i 8.12 iz skripte](http://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf)
    - Za deo koji se tiče rada sa `Observable` tokovima, pogledajte poglavlje 4, sa akcentom na reaktivno programiranje i biblioteku RxJS koju Angular koristi intenzivno na različitim mestima (pre svega za rad sa HTTP zahtevima).
    - Dodatno, (za domaći) možete pogledati predavanje posvećeno ovoj temi koje prati tekst iz skripte: [Youtube link](https://www.youtube.com/watch?v=3QC45tpjwRk)

- [Aplikacija sa časa](./store-spa/). Izgled aplikacije:

![Store SPA](./app-images/store-spa3-1.png)
![Store SPA](./app-images/store-spa3-2.png)
![Store SPA](./app-images/store-spa3-3.png)
![Store SPA](./app-images/store-spa3-4.png)
![Store SPA](./app-images/store-spa3-5.png)
![Store SPA](./app-images/store-spa3-6.png)
![Store SPA](./app-images/store-spa3-7.png)
