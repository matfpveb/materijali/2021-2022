# Finalna aplikacija - prodavnica

- [Node.js server za prodavnicu](./store-server/)
    - [Informacije o serveru](./store-server/README.md)
- [Početni podaci za popunjavanje BP](./store-data)
    - Nakon preuzimanja koda, potrebno je izvršiti naredbe:
        - `chmod +x import-store.sh`
        - `./import-store.sh`
- [Finalna aplikacija](./store-spa/). 
