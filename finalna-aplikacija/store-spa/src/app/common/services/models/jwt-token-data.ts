export interface IJWTTokenData {
  id: string;
  username: string;
  email: string;
  name: string;
  imgUrl: string;
}
