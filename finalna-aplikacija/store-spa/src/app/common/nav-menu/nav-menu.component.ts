import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/users/services/auth.service";
import { User } from "src/app/users/models/user";
import { Subscription } from "rxjs";

@Component({
  selector: "app-nav-menu",
  templateUrl: "./nav-menu.component.html",
  styleUrls: ["./nav-menu.component.css"],
})
export class NavMenuComponent implements OnDestroy {
  user: User;
  userSub: Subscription;

  constructor(private authService: AuthService) {
    this.userSub = this.authService.user.subscribe((user: User) => (this.user = user));
    this.authService.sendUserDataIfExists();
  }

  public ngOnDestroy(): void {
    this.userSub ? this.userSub.unsubscribe() : null;
  }

  public getLoginOrLogoutRouteLink(): string {
    return this.user ? "/logout" : "/login";
  }
}
