import { Injectable } from "@angular/core";
import { Product, IProduct } from "src/app/products/models/product";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { JwtService } from "src/app/common/services/jwt.service";
import { Observable } from "rxjs";
import { Order, IOrder } from "../models/order";
import { map, tap } from "rxjs/operators";
import { IUser } from "src/app/users/models/user";

// Domaci zadatak: Dopuniti implementaciju tako da se podaci o proizvodima koje je korisnik odabrao cuvaju i kada se zatvori veb pregledac.

@Injectable({
  providedIn: "root",
})
export class OrderService {
  private basket: Product[] = [];

  private urls = {
    postOrder: "http://localhost:3000/api/orders",
    getOrders: "http://localhost:3000/api/orders",
    getOrderById: "http://localhost:3000/api/orders",
  };

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  public addProductToBasket(product: Product): boolean {
    const alreadyAdded: boolean = this.basket.findIndex((p: Product) => p.id === product.id) !== -1;
    if (!alreadyAdded) {
      this.basket.push(product);
    }
    return alreadyAdded;
  }

  public get productsInBasket(): Product[] {
    return this.basket;
  }

  public get isBasketEmpty(): boolean {
    return this.basket.length === 0;
  }

  public removeProductFromBasket(product: Product): void {
    const index: number = this.basket.findIndex((p: Product) => p.id === product.id);
    if (index === -1) {
      return;
    }
    this.basket.splice(index, 1);
  }

  public postOrder(): Observable<Order> {
    const body = {
      userId: this.jwtService.getDataFromToken().id,
      productsIds: this.basket.map((product: Product) => product.id),
    };
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http.post<IOrder>(this.urls.postOrder, body, { headers }).pipe(
      tap(() => (this.basket = [])),
      map((pojo: IOrder) => this.convertOrderPOJOToOrder(pojo, false, false)),
    );
  }

  public getOrders(): Observable<Order[]> {
    const params: HttpParams = new HttpParams().append("userId", this.jwtService.getDataFromToken().id);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .get<IOrder[]>(this.urls.getOrders, { headers, params })
      .pipe(map((pojos: IOrder[]) => pojos.map((pojo: IOrder) => this.convertOrderPOJOToOrder(pojo, false, false))));
  }

  public getOrderById(orderId: string): Observable<Order> {
    const params: HttpParams = new HttpParams().append("userId", this.jwtService.getDataFromToken().id);
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .get<IOrder>(`${this.urls.getOrderById}/${orderId}`, { headers, params })
      .pipe(map((pojo: IOrder) => this.convertOrderPOJOToOrder(pojo, true, true)));
  }

  private convertOrderPOJOToOrder(pojo: IOrder, fetchedProducts: boolean, fetchedUser: boolean): Order {
    return new Order(
      pojo._id,
      fetchedProducts ? [] : (pojo.products as string[]),
      fetchedProducts ? (pojo.products as IProduct[]) : [],
      fetchedUser ? "" : (pojo.user as string),
      fetchedUser ? (pojo.user as IUser) : null,
      pojo.orderTimestamp,
    );
  }
}
