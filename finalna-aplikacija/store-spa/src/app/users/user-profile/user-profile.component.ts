import { Component, OnDestroy } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { User } from "../models/user";
import { Subscription } from "rxjs";
import { UserService } from "../services/user.service";
import { AuthService } from "../services/auth.service";

// Ovaj interfejs cemo koristiti za tipiziranost podataka koje je korisnik uneo u formularu
interface IUserFormValue {
  username: string;
  email: string;
  name: string;
}

@Component({
  selector: "app-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"],
})
export class UserProfileComponent implements OnDestroy {
  public user: User;
  public showChangeFields: boolean;

  // Model za formular
  public userForm: FormGroup;
  // Informacija o datoteci koji je korisnik odabrao sa diska
  private imageToUpload: File;

  // Niz pretplata na razne tokove.
  // Videti onSubmitUserForm() ispod za vise informacija.
  private activeSubscriptions: Subscription[] = [];

  public constructor(private userService: UserService, private authService: AuthService) {
    this.showChangeFields = false;
    const userSub: Subscription = this.authService.user.subscribe((user: User) => {
      this.user = user;
      this.initializeUserFormGroup();
    });
    this.authService.sendUserDataIfExists();
    this.activeSubscriptions.push(userSub);
  }

  public ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  public logout(): void {
    this.authService.logoutUser();
  }

  private initializeUserFormGroup(): void {
    this.userForm = new FormGroup({
      username: new FormControl(this.user.username, [Validators.required, Validators.pattern(/[a-zA-Z0-9_-]{8,}/)]),
      email: new FormControl(this.user.email, [Validators.required, Validators.email]),
      name: new FormControl(this.user.name, [Validators.required]),
      imgUrl: new FormControl(""),
    });
  }

  public enableChangeFields(): void {
    this.showChangeFields = true;
  }

  // File chooser, iako deo formulara, nece sadrzati pune informacije o datoteci, vec samo "laznu putanju",
  // pa zato moramo da zapamtimo detaljne informacije o odabranoj datoteci.
  public handleFileInput(event: Event): void {
    const files: FileList = (event.target as HTMLInputElement).files;
    if (!files.length) {
      return;
    }
    this.imageToUpload = files.item(0);
  }

  onSubmitUserForm(): void {
    if (this.userForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    if (!this.imageToUpload) {
      const response: boolean = window.confirm(
        "You haven't chosen a new profile picture. Do you want to skip this field?",
      );
      if (!response) {
        return;
      }
    }

    const formData: IUserFormValue = this.userForm.value;

    // Primetimo da smo resetovanje formulara i ostalu obradu
    // nakon podnosenja formulara stavili u subscribe() metod.
    // Da smo to ostavili ispod poziva patchUserData(),
    // onda bi se formular resetovao pre nego sto se dobije HTTP odgovor.
    const sub: Subscription = this.userService
      .patchUserData(formData.username, formData.email, formData.name)
      .subscribe((user: User) => {
        if (!user) {
          return;
        }

        this.user = user;

        this.userForm.reset({
          username: this.user.username,
          email: this.user.email,
          name: this.user.name,
          imgUrl: "",
        });

        if (this.imageToUpload) {
          // Losa je praksa pozivati subscribe() u okviru drugog subscribe().
          // Videti kako se ovo resava u ProductService-u.
          const sub: Subscription = this.userService
            .patchUserProfileImage(this.imageToUpload)
            .subscribe((user: User) => {
              this.user = user;
              this.imageToUpload = null;
            });
          this.activeSubscriptions.push(sub);
        }

        this.showChangeFields = false;
      });
    // S obzirom da korisnik moze vise puta da poziva ovaj metod,
    // ova komponenta moze tokom svog zivotnog ciklusa napraviti nekoliko pretplata.
    // Zbog toga, svaku pretplatu cuvamo u niz activeSubscription,
    // a kada se komponenta unisti, mi ukidamo pretplate za sve njih.
    this.activeSubscriptions.push(sub);
  }
}
