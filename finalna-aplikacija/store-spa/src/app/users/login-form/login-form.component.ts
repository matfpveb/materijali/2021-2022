import { Component, OnDestroy } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.css"],
})
export class LoginFormComponent implements OnDestroy {
  loginForm: FormGroup;
  loginSub: Subscription;

  constructor(private authService: AuthService, private router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{8,}$/)]),
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
    });
  }

  public ngOnDestroy(): void {
    this.loginSub ? this.loginSub.unsubscribe() : null;
  }

  public login(): void {
    if (this.loginForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const formData = this.loginForm.value;
    this.loginSub = this.authService.loginUser(formData.username, formData.password).subscribe(() => {
      this.router.navigateByUrl("/");
    });
  }
}
