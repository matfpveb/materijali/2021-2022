import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UserProfileComponent } from "./users/user-profile/user-profile.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ProductListComponent } from "./products/product-list/product-list.component";
import { ProductComponent } from "./products/product/product.component";
import { CreateProductComponent } from "./products/create-product/create-product.component";
import { LoginFormComponent } from "./users/login-form/login-form.component";
import { RegisterFormComponent } from "./users/register-form/register-form.component";
import { NavMenuComponent } from "./common/nav-menu/nav-menu.component";
import { LogoutComponent } from "./users/logout/logout.component";
import { ProductDetailsComponent } from "./products/product-details/product-details.component";
import { ShoppingCartComponent } from "./orders/shopping-cart/shopping-cart.component";
import { OrderListComponent } from "./orders/order-list/order-list.component";
import { MyOrderComponent } from "./orders/my-order/my-order.component";
import { ProductListPaginationComponent } from "./products/product-list-pagination/product-list-pagination.component";
import { ProductSumPipe } from './products/pipes/product-sum.pipe';
import { ProductSortPipe } from './products/pipes/product-sort.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UserProfileComponent,
    ProductListComponent,
    ProductComponent,
    CreateProductComponent,
    LoginFormComponent,
    RegisterFormComponent,
    NavMenuComponent,
    LogoutComponent,
    ProductDetailsComponent,
    ShoppingCartComponent,
    OrderListComponent,
    MyOrderComponent,
    ProductListPaginationComponent,
    ProductSumPipe,
    ProductSortPipe,
  ],
  imports: [BrowserModule, AppRoutingModule, ReactiveFormsModule, FormsModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
