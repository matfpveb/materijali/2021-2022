import { Component, OnInit, Output, EventEmitter, OnDestroy } from "@angular/core";
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { Observable, Subscription, forkJoin } from "rxjs";
import { Product } from "../models/product";
import { switchMap } from "rxjs/operators";
import { ProductState } from "../models/product-state";
import { ProductNameValidator } from "../validators/product-name.validator";
import { ProductService } from "../services/product.service";
import { CreateProductDTO } from "../services/models/create-product-dto";
import { HttpEvent, HttpEventType } from "@angular/common/http";
declare const $: any;

type ProgressInfo = { fileName: string; label: string };

interface ICreateProductFormValue {
  name: string;
  price: number;
  description: string;
  forSale: boolean;
  state: ProductState;
  files: string;
}

@Component({
  selector: "app-create-product",
  templateUrl: "./create-product.component.html",
  styleUrls: ["./create-product.component.css"],
})
export class CreateProductComponent implements OnInit, OnDestroy {
  ProductStateEnum = ProductState;

  @Output()
  public productCreated: EventEmitter<Product> = new EventEmitter<Product>();

  // Formular za kreiranje novog proizvoda
  public createProductForm: FormGroup;
  // Informacije o slikama proizvoda koje se pohranjuju
  private selectedFiles: FileList;
  public progressInfos: ProgressInfo[] = [];

  // Aktivne pretplate na tokove
  private activeSubscriptions: Subscription[] = [];

  public constructor(private productService: ProductService) {
    this.initializeCreateProductForm();
  }

  public ngOnInit(): void {
    $(".ui.checkbox").checkbox();
    $(".ui.radio.checkbox").checkbox();
  }

  public ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

  public initializeCreateProductForm(): void {
    this.createProductForm = new FormGroup({
      name: new FormControl("", [Validators.required, Validators.minLength(10), ProductNameValidator]),
      price: new FormControl("", [Validators.required, Validators.min(0.99)]),
      description: new FormControl("", [Validators.required, Validators.minLength(50)]),
      forSale: new FormControl(true, Validators.required),
      state: new FormControl(null, Validators.required),
    });
  }

  // Metod koji se poziva kada korisnik odabere datoteke
  selectFiles(event: Event): void {
    this.progressInfos = [];
    this.selectedFiles = (event.target as HTMLInputElement).files;
  }

  // Validacija formulara
  nameHasErrors(): string {
    return this.fieldHasErrors("name") ? "error" : "";
  }

  priceHasErrors(): string {
    return this.fieldHasErrors("price") ? "error" : "";
  }

  descriptionHasErrors(): string {
    return this.fieldHasErrors("description") ? "error" : "";
  }

  forSaleHasErrors(): string {
    return this.fieldHasErrors("forSale") ? "error" : "";
  }

  stateHasErrors(): string {
    return this.fieldHasErrors("state") ? "error" : "";
  }

  nameErrors(): string[] {
    const errorMessages: string[] = [];
    const errors: ValidationErrors = this.createProductForm.get("name").errors;
    if (!errors) {
      return errorMessages;
    }

    if (errors.required) {
      errorMessages.push("Product must have a name.");
    }
    if (errors.minlength) {
      errorMessages.push(
        `Product name must have at least ${errors.minlength.requiredLength} characters. ` +
          `You have typed in ${errors.minlength.actualLength} characters.`,
      );
    }
    if (errors.productName) {
      errorMessages.push(errors.productName.message);
    }
    return errorMessages;
  }

  private fieldHasErrors(fieldName: string): boolean {
    const errors: ValidationErrors = this.createProductForm.get(fieldName).errors;
    return errors != null;
  }

  // Slanje podataka iz formulara
  public createProduct(): void {
    // Postoji i polje invalid koje ce biti true ako formular ne prolazi validaciju
    if (this.createProductForm.invalid) {
      window.alert("The form is not valid!");
      return;
    }

    const data = this.createProductForm.value as ICreateProductFormValue;
    const productDTO: CreateProductDTO = new CreateProductDTO();
    productDTO.name = data.name;
    productDTO.price = data.price;
    productDTO.description = data.description;
    productDTO.forSale = data.forSale;
    productDTO.state = data.state;

    // Bez slanja slika:
    // const sub: Subscription = this.productService.postProduct(productDTO).subscribe((product: Product) => {
    //   this.createProductForm.reset({ forSale: true });
    // });
    // this.activeSubscriptions.push(sub);

    // Sa slanjem slika:
    const sub: Subscription = this.productService
      .postProduct(productDTO)
      .pipe(
        // Operator switchMap se koristi za "promenu" toka na koji se pretplacujemo.
        // Kada se emituje vrednost u izvornom toku, poziva se funkcija koja se prosledjuje ovom operatoru,
        // za koju se ocekuje da vraca novi tok (najcesce sa novom vrednoscu koja se emituje).
        switchMap((product: Product) => {
          this.createProductForm.reset({ forSale: true });
          return this.uploadFiles(product.id);
        }),
      )
      .subscribe((events: HttpEvent<FormData>[]) => {
        console.log(JSON.stringify(events));
        events.forEach((event: HttpEvent<FormData>, index: number) => {
          let percent: number = 0;
          let label: string = "";
          const file: File = this.selectedFiles.item(index);
          const progressInfo: ProgressInfo = this.progressInfos[index];

          switch (event.type) {
            case HttpEventType.Sent:
              percent = 0;
              label = `Uploading file "${file.name}" of size ${file.size}.`;
              break;

            case HttpEventType.UploadProgress:
              percent = this.calculateProgressPercentage(event.loaded, event.total);
              label = `File "${file.name}" is ${percent}% uploaded.`;
              break;

            case HttpEventType.Response:
              percent = 100;
              label = `File "${file.name}" was completely uploaded!`;
              break;

            default:
              label = `File "${file.name}" surprising upload event: ${event.type}.`;
              break;
          }

          progressInfo.label = label;
          this.progressBar(index, percent);
        });
      });
    this.activeSubscriptions.push(sub);
  }

  // Metod kojim cemo asinhrono slati sve slike na server
  private uploadFiles(productId: string): Observable<HttpEvent<FormData>[]> {
    const uploadObservables: Observable<HttpEvent<FormData>>[] = [];
    for (let i = 0; i < this.selectedFiles.length; i++) {
      const file: File = this.selectedFiles.item(i);
      const observable: Observable<HttpEvent<FormData>> = this.productService.putProductImage(productId, file);
      uploadObservables.push(observable);

      const progressInfo: ProgressInfo = { fileName: file.name, label: "" };
      this.progressInfos[i] = progressInfo;
    }
    return forkJoin(uploadObservables);
  }

  // Pomocne funkcije
  private calculateProgressPercentage(dataValue: number, dataTotal: number): number {
    return Math.round((dataValue / dataTotal) * 100);
  }

  private progressBar(i: number, percent: number): void {
    const progressBar = $(".ui.progress")[i];
    $(progressBar).progress({ percent });
  }
}
