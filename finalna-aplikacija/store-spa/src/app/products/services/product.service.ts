import { HttpClient, HttpParams, HttpHeaders, HttpEvent, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Product, IProduct } from "../models/product";
import { IGetProductsResponseBody, GetProductsResponseBody } from "./models/get-products-response-body";
import { CreateProductDTO } from "./models/create-product-dto";
import { JwtService } from "src/app/common/services/jwt.service";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  private urls = {
    getProducts: "http://localhost:3000/api/products/",
    postProduct: "http://localhost:3000/api/products/",
    putProductImage: "http://localhost:3000/api/products/upload/",
    getProductDetailsById: "http://localhost:3000/api/products/",
  };

  constructor(private http: HttpClient, private jwtService: JwtService) {}

  // Domaci zadatak: Dopuniti implementaciju servisa tako da se obradjuju HTTP greske kao u drugim servisima

  public getProductsWithPagination(page: number = 1, limit: number = 10): Observable<GetProductsResponseBody> {
    const queryParams: HttpParams = new HttpParams().append("page", page.toString()).append("limit", limit.toString());
    return this.http
      .get<IGetProductsResponseBody>(this.urls.getProducts, {
        params: queryParams,
      })
      .pipe(map((paginationPOJO: IGetProductsResponseBody) => this.createPaginationFromPOJO(paginationPOJO)));
  }

  public postProduct(productDTO: CreateProductDTO): Observable<Product> {
    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);
    return this.http
      .post<IProduct>(this.urls.postProduct, productDTO, { headers })
      .pipe(map((productPOJO: IProduct) => this.createProductFromPOJO(productPOJO)));
  }

  public putProductImage(productId: string, file: File): Observable<HttpEvent<FormData>> {
    // Datoteke ne mozemo da saljemo "tek tako",
    // vec ih moramo serijalizovati kao deo FormData
    const formData: FormData = new FormData();
    formData.append("file", file);

    const headers: HttpHeaders = new HttpHeaders().append("Authorization", `Bearer ${this.jwtService.getToken()}`);

    // Ovde koristimo malo drugaciji pristup slanja HTTP zahteva koji smo videli do sada.
    // Umesto da osluskujemo odgovor od servera, ovde pratimo dogadjaje koji se emituju prilikom slanja datoteke.
    // Ovih dogadjaja ima mnogo, a HttpEvent<T> predstavlja uniju svih njih.
    // Pogledati vise o ovome na: https://angular.io/guide/http#tracking-and-showing-request-progress.
    const req: HttpRequest<FormData> = new HttpRequest<FormData>(
      "PUT", // HTTP metod
      this.urls.putProductImage + productId, // URL
      formData, // Telo HTTP zahteva
      // Opcije HTTP zahteva
      {
        headers,
        // Oznacavamo da zelimo da dobijamo informacije o svakom dogadjaju prilikom pohranjivanja datoteke
        reportProgress: true,
      },
    );
    return this.http.request<FormData>(req);
  }

  public getProductDetailsById(productId: string): Observable<Product> {
    return this.http
      .get<IProduct>(`${this.urls.getProductDetailsById}/${productId}`)
      .pipe(map((pojo: IProduct) => this.createProductFromPOJO(pojo)));
  }

  private createPaginationFromPOJO(pojo: IGetProductsResponseBody): GetProductsResponseBody {
    const pagination: GetProductsResponseBody = new GetProductsResponseBody();
    pagination.docs = pojo.docs.map((doc: IProduct) => this.createProductFromPOJO(doc));
    pagination.hasNextPage = pojo.hasNextPage;
    pagination.hasPrevPage = pojo.hasPrevPage;
    pagination.limit = pojo.limit;
    pagination.nextPage = pojo.nextPage;
    pagination.page = pojo.page;
    pagination.pagingCounter = pojo.pagingCounter;
    pagination.prevPage = pojo.prevPage;
    pagination.totalDocs = pojo.totalDocs;
    pagination.totalPages = pojo.totalPages;
    return pagination;
  }

  private createProductFromPOJO(pojo: IProduct): Product {
    return new Product(
      pojo._id,
      pojo.name,
      pojo.description,
      pojo.price,
      pojo.forSale,
      pojo.state,
      pojo.owner,
      pojo.imgUrls,
      pojo.stars,
    );
  }
}
