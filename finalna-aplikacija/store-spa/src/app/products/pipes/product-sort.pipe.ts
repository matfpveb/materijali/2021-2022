import { Pipe, PipeTransform } from "@angular/core";
import { IProduct, Product } from "../models/product";

@Pipe({
  name: "productSort",
})
export class ProductSortPipe implements PipeTransform {
  transform(
    products: (IProduct | Product)[],
    fieldToSortBy: keyof (IProduct | Product),
    order: "ASC" | "DESC" = "ASC",
  ): (IProduct | Product)[] {
    const productsCopy = [...products];
    return productsCopy.sort((left: IProduct | Product, right: IProduct | Product) => {
      if (left[fieldToSortBy] < right[fieldToSortBy]) {
        return order === "ASC" ? -1 : 1;
      }
      if (left[fieldToSortBy] > right[fieldToSortBy]) {
        return order === "ASC" ? 1 : -1;
      }
      return 0;
    });
  }
}
