import { Pipe, PipeTransform } from "@angular/core";
import { Product, IProduct } from "../models/product";

@Pipe({
  name: "productSum",
})
export class ProductSumPipe implements PipeTransform {
  transform(products: (IProduct | Product)[]): number {
    return products
      .map((product: IProduct | Product) => product.price)
      .reduce((left: number, right: number) => left + right, 0);
  }
}
