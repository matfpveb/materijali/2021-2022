export enum ProductState {
  New = "New",
  BarelyUsed = "Barely Used",
  ModeratelyUsed = "Moderately Used",
}
