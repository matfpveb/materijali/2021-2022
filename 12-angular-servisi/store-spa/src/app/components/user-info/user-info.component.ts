import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit, OnDestroy {
  public sub: Subscription;
  public user: User | null = null;
  public isLogin: boolean = false;

  constructor(private auth: AuthService) {
    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });

    this.auth.sendUserDataIfExists();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  toggleLogin(): void {
    this.isLogin = !this.isLogin;
  }

}
