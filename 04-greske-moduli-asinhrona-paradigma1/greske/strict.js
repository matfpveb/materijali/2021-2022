// 1)
// 'use strict';
// x = 10;
// console.log(x);

// 2)
// 'use strict';
// var b = 5;
// function f(a, a, c){
//     return a + b + c;
// }

// console.log(f(1, 2, 3));

// 3)
'use strict';
let obj = {}
Object.defineProperty(obj, 'x', {
    value: 5, 
    writable: false
});
console.log(obj.x);
obj.x = 10;
console.log(obj.x);
