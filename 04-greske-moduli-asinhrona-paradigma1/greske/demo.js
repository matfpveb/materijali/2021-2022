class FormatError extends Error {
  constructor(...params) {
    super(...params);
    this.time = new Date();
  }
}

try {
  const s = 'hello!';
  if (s[0].toLowerCase() == s[0]) {
    throw new FormatError('Wrong format!');
  }
} catch (e) {
  console.log(e.name);
  console.log(e.message);
  console.log(e.time);
}
