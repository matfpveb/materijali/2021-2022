// V1
// exports.add = (a, b) => a + b;
// exports.mul = (a, b) => a * b;
// exports.PI = 3.14;

// V2
const add_numbers = (a, b) => a + b;
const mul_numbers = (a, b) => a * b;
const PI = 3.14;

module.exports = {
  add: add_numbers,
  mul: mul_numbers,
  PI,
};
