# 4. sedmica vežbi

## JavaScript

- Sekcije 2.6, 2.7 i 2.8 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}
- Napomena: _Primere otvarati u veb pregledaču putem HTTP protokola, na primer, korišćenjem `Live server` ekstenzije za VS Code_

## Dodatni korisni resursi

- [Podrška za ECMAScript module u Node.js platformi](https://blog.logrocket.com/es-modules-in-node-js-12-from-experimental-to-release/){:target="_blank"}
- Zašto asinhroni HTTP zahtev može da "ne uspe"? Verovatno je rešenje u CORS-u:
    - Šta je CORS?
        - [Ukratko o CORS-u](https://www.codecademy.com/articles/what-is-cors){:target="_blank"}
        - [Detaljnije o CORS-u](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS){:target="_blank"}
        - [Razni dodatni resursi](https://enable-cors.org/resources.html){:target="_blank"}
    - CORS-aware implementacija na klijentu
        - [Ako se koristi XMLHttpRequest API](https://www.html5rocks.com/en/tutorials/cors/){:target="_blank"}
        - [Ako se koristi Fetch API](https://javascript.info/fetch-crossorigin){:target="_blank"}
    - CORS-aware implementacija na serveru
        - [Tutorijali kako se implementira CORS za razna serverska okruženja](https://enable-cors.org/server.html){:target="_blank"}
    - Detaljniji pristup CORS-u se može pronaći u knjizi na [ovoj vezi](https://www.manning.com/books/cors-in-action){:target="_blank"}. Na toj vezi je dostupan izvorni kod svih primera iz knjige koji se može besplatno preuzeti.
