const message = 'Hello there!';

function second() {
  console.log(message);
}

function first() {
  const message = 'Hi there!';
  console.log(message);

  second();

  function third() {
    console.log(message);
  }

  third();
}

first();
