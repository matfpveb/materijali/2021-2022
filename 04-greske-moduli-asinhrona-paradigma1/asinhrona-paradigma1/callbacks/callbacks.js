const simplePrint = (message) => {
  console.log(message);
};

const prettyPrint = (message) => {
  console.log(`*~* ${message} *~*`);
};

const showMessage = (a, b, callback) => {
  const result = a + b;

  if (result > 10) {
    callback(result);
  } else {
    simplePrint(result);
  }
};

showMessage(2, 11, prettyPrint);
