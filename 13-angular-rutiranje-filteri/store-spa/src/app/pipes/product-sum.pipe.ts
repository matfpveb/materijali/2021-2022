import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../models/product.model';

@Pipe({
  name: 'productSum'
})
export class ProductSumPipe implements PipeTransform {

  transform(products: Product[], defaultValue: number = 0): number {
    if (products.length === 0) {
        return defaultValue;
    }
    return products.map((product: Product) => product.price).reduce((left: number, right: number)=> left + right); 
  }
}
