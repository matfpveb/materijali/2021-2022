import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserAuthenticatedGuard } from './guards/user-authenticated.guard';

const routes: Routes = [
  // /
  {path: '', component: ProductListComponent},
  // /register
  { path: 'register', component: RegistrationFormComponent},
  // login
  { path: 'login', component: LoginFormComponent},
  // logout
  { path: 'logout', component: LogoutComponent},
  // user
  { path: 'user', component: UserProfileComponent, canActivate: [UserAuthenticatedGuard]},
  { path: 'create-product', component: CreateProductComponent, canActivate: [UserAuthenticatedGuard]},
  { path: 'products/:productId', component: ProductDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
