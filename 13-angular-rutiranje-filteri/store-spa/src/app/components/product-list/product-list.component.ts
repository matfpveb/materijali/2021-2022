import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list', 
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  @Input() products: Observable<Product[]> = {} as Observable<Product[]>;

  constructor(private productService: ProductService) {
    this.products = this.productService.getProducts(1, 10);
  }

  ngOnInit(): void {
  }

}
