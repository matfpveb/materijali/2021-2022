import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Product } from 'src/app/models/product.model';
import { AuthService } from 'src/app/services/auth.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  private sub: Subscription = new Subscription();
  public product: Observable<Product> = new Observable<Product>();

  constructor(
    private productService: ProductService,
    private auth: AuthService,
    private activatedRoute: ActivatedRoute
  ) {
    this.sub = this.activatedRoute.paramMap.subscribe((params: ParamMap) => {
      const productId: string | null = params.get('productId');
      console.log(productId);
      console.log(params);
      this.product = this.productService.getProductById(productId!);
    });

    this.auth.sendUserDataIfExists();

    // this.product = this.activatedRoute.paramMap.pipe(
    //   switchMap((params: ParamMap) => {
    //       const productId: string | null = params.get('productId');
    //       return this.productService.getProductById(productId!);
    //   })
    // );

    // this.auth.sendUserDataIfExists();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

}
