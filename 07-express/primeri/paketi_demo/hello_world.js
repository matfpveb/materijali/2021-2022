const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

app.get('/', (req, res) => {
  res.send('Zdravo svima!');
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na portu ${port} i adresi localhost`);
});