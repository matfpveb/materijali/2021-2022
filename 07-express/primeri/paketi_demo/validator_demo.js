const validator = require('validator');

// na nivou paketa validator su podrzane funkcije za validaciju
// i sanitetizaciju ulaza

// neke od podrzanih validaicja: elektronske adrese, datumi, brojevi kreditnih
// kartica, valute, URL adrese, numerickih zapias, ...

// validacija elektronskih adresa
const email = 'test@gmail.com';
const isValidEmail = validator.isEmail(email);
console.log('isValidEmail: ', isValidEmail);

// validacija sadrzaja
const username = 'markopopovic';
const isValidUsername = validator.isAlphanumeric(username);
console.log('isValidUsername: ', isValidUsername);

// validacija datuma
const birthday = '2021-07-32';
const isValidBirthday = validator.isDate(birthday);
console.log('isValidBirthday: ', isValidBirthday);


