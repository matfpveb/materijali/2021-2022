const jwt = require('jsonwebtoken');

const adminPassword = process.env.ADMIN_PASSWORD || 'adminpass';

const jwtSecret = process.env.JWT_SECRET || 'pveb_showtime';
const jwtOpts = { algorithm: 'HS256', expiresIn: '30d' };

const data = {
    adminUsername: 'admin', 
    adminPassword: adminPassword
};

// za kreiranje tokena koristi se funkcija sign
const token = jwt.sign(data, jwtSecret, jwtOpts);
console.log('Token: ', token);

// dobijenu vrednost tokena
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pblVzZXJuYW1lIjoiYWRtaW4iLCJhZG1pblBhc3N3b3JkIjoiYWRtaW5wYXNzIiwiaWF0IjoxNjE3NzM4NzEzLCJleHAiOjE2MjAzMzA3MTN9.h-H96EdSvm_q6PFrKrjPoi-c5akNVgDynrrq1bTblIw
// cemo zapamtiti zbog kasnijeg slanja serveru koriscenjem
// 'x-access-token' polja; ukoliko ovo polje bude izostavljeno prijavicemo 
// neautorizovani pristup (kod 401)

// za izdvajanje informacija iz tokena koristi se funkcija verify
const payloadData = jwt.verify(token, jwtSecret)
console.log(payloadData);


