const users = require('./users');

let user;

user = users.getUserById('53eba886-e0dc-4bc6-9a75-89f66ffc0cef');
console.log('By id: ', user);

user = users.getUserByUsername('pavle');
console.log('By username: ', user);

const activeUsers = users.getUsersByStatus('active');
console.log('Active users: ', activeUsers);

// users.addNewUser('marko', 'marko@gmail.com', 'snow_time');
// console.log(users.getAllUsers());
