const express = require('express');
const usersRouter = require('./routes/api/users');
const { urlencoded, json } = require('body-parser');

const app = express();

app.use(json());
app.use(urlencoded({ extended: false }));

app.use('/api/users', usersRouter);

app.use(function (req, res, next) {
  const error = new Error('Method not Allowed!');
  error.status = 405;

  next(error);
});

app.use(function (error, req, res, next) {
  const statusCode = error.status || 500;
  res.status(statusCode).json({
    error: {
      message: error.message,
      status: statusCode,
      stack: error.stack,
    },
  });
});

module.exports = app;
