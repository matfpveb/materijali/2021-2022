# 7. sedmica vežbi

## Node.js - Kreiranje serverskih aplikacija pomoću Express.js radnog okvira

- [Primeri sa časa](./primeri/)
- [Express.js tekst](./expressjs.md)
    - Ovo je preliminarna verzija teksta (od prošle godine). Kada budemo ažurirali skriptu, tada ćemo ažurirati i ovaj link.
    - Neki primeri iz teksta se malo razlikuju u odnosu na one koje smo radili na času
