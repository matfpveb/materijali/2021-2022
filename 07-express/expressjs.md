# Express.js

U prethodnom poglavlju smo upoznali `http` modul i njegovu podršku u implementaciji veb servera. Kako je ovo čest zadatak modernih veb aplikacija, u ovom poglavlju ćemo otići korak dalje i upoznati radni okvir Express.js koji će rad na ovim zadacima učiniti još preglednijim i jednostavnijim.

[Express.js](https://expressjs.com/) je najpopularniji veb okvir (engl. web framework) Node.js zajednice. Svojim dizajnom i skupom raspoloživih funkcija nudi mehanizme za udoban razvoj aplikativnih programskih interfejsa u duhu REST arhitekture, integraciju sa mašinama za prikazivanje šablona, dodatne obrade kroz koncept srednjeg sloja i podešavanja okruženja samih aplikacija.

Zvanična zajednica Express.js radni okvir opisuje i kao brz, blagog karaktera (engl. unopinionated) i minimalistički orijentisani. Blag karakter ovog okruženja se oslikava kroz skroman broj restrikcija koje se odnose na strukturu i načine uvezivanja komponenti u funkcionalne aplikacije. Kada je reč o minimalističkoj prirodi Express.js, ona zaista stoji. Jezgro radnog okvira podržava samo neophodne funkcionalnosti, a zajednica je ta koja je kroz koncept srednjeg sloja (engl. middleware) okruženja nadgradila ovo ponašanje. Zahvaljujući njima postoje olakšice u radu sa sesijama, logovanjima korisnika i autorizacijama, kolačićima, analizom URL parametara i mnoge druge.

# REST API

Napomenuli smo da je jedan od primarnih zadataka radnog okvira Express.js razvoj aplikativnih programskih interfejsa (APIja) u duhu REST arhitekture. U ovoj sekciji ćemo približiti šta tačno ovi koncepti znače i kako ih možemo koristiti pri projektovanju naših serverskih aplikacija.

Osnovno načelo REST (engl. REspresentational State Transfer) arhitekture je da klijentski i serverski deo veb aplikacije komuniciraju razmenom HTTP zahteva i HTTP odgovora nalik komunikaciji koju vode pregledači i veb serveri pri isporučivanju veb sadržaja.

Baš kao i na vebu, praksa je da se entiteti nad kojima aplikacija manipuliše nazivaju resursima. Na primer, resurs može biti korisnik aplikacije, proizvod koji aplikacija nudi ili kolekcija takvih proizvoda, potrošačka korpa korisnika ili statistika o posećenosti koja se održava. Resursima kolekcijama ponajčešće, na fizičkom nivou, odgovaraju istoimeni entiteti baze podataka (na primer, tabele), a pojedinačnim resursima njihovi pojedinačni unosi (na primer, redovi tabele).

REST arhitektura podrazumeva da se resursi opisuju URL adresama. Na primer, ako je aplikacija aktivna na adresi http://localhost, URL adresa http://localhost/users se može upariti sa kolekcijom resursa _users_, a URL adresa http://localhost/products sa kolekcijom resursa _products_. Pojedinačnim resursima ovih kolekcija bi mogle odgovarati, na primer, adrese http://localhost/users/bane i http://localhost/products/no123 u kojima dodaci _/bane_ i _/no123_ predstavljaju vrednosti poput korisničkog imena ili koda proizvoda kojima se oni mogu jedinstveno identifikovati. U URL adresama se mogu naći i parametri pretrage (engl. query parametars). Njima se pojančešće sužava kolekcija resursa. Na primer, navođenjem parametra pretrage _status_ može se postići da URL adresa http://localhost/users?status=active predstavlja samo jedan deo kolekcije korisnika i to onih koji su aktivni. Ovde je važno naglasiti da su pomenute URL adrese logičke i da ne zahtevaju postojanje fizičkih datoteka koje ih prate. To nam značajno olakšava rad i organizaciju velikih projekata.

Kolekcija svih podržanih URL adresa jednog servera predstavlja njegov API, a pojedinačne adrese se zovu i krajnje tačke (engl. endpoints).

Željene radnje nad resursima se u duhu REST arhitekture opisuju HTTP metodama. Zahtevu tipa GET se pridružuje operacija čitanja, zahtevu tipa POST operacija kreiranja, zahtevu tipa PUT operacija ažuriranja, a zahtevu tipa DELETE operacija brisanja. Tako se, na primer, slanjem HTTP zahteva tipa GET ka resursu http://localhost/users može inicirati operacija čitanja informacija o svim korisnicima, a slanjem DELETE zahteva ka resursu http://localhost/users/bane operacija brisanja korisnika sa korisničkim imenom _bane_. Da bi se kompletirali zahtevi kreiranja i ažuriranja potrebni su nam i podaci o novim resursima ili o vrednostima koje treba promeniti. Praksa je da se ovi podaci šalju kroz telo HTTP zahteva. Uobičajeni format slanja podataka je JSON.

REST arhitektura podrazumeva i da se statusnim kodom odgovora klijent kratko obavesti o uspešnosti zahtevane radnje. Tako, na primer, na GET zahtev klijenta, server može odgovoriti statusnim kodom 200 (sa značenjem *OK*) ukoliko je sve u redu, statusnim kodom 404 (sa značenjem *File Not Found*) ukoliko zahtevani resurs nije pronađen ili statusnim kodom 400 (sa značenjem *Bad Request*) ukoliko u zahtevu nisu prisutne sve potrebne informacije. U telu odgovora bi se u ovom slučaju poslale trenutne reprezentacije zahtevanog resursa ili kolekcije resursa, najčešće u JSON formatu. Za zahteve drugih tipova, kao što su POST ili UPDATE, praksa je da se u telu odgovora vrate novokreirane tj. izmenjene vrednosti resursa, dok se u slučaju zahteva tipa DELETE praktikuje da telo odgovora ostane prazno.

Aplikativni programski interfejski koji se razvijaju na ovaj način su javno dokumentovani kako bi klijenti koji žele da ih koriste precizno znali semantiku zahteva i podatke koji ih prate. Na narednoj slici prikazan je fragmet YouTube video REST API-ja koji se odnosi na rad sa video sadržajima.
![YouTube video API](./assets/YouTube_video_API.png)

Slanjem zahteva tipa GET ka resursu `https://www.googleapis.com/youtube/v3/videos?id=EG16dFYK0gw&key=API_KEY` se mogu dobiti sve informacije o videu čiji je identifikator _dqiA1gVp57_. Ovaj API zahteva i autorizaciju korisnika, pa možemo videti da se pored parametra _id_ očekuje i vrednost ključa _key_ za pristup. Detaljan opis ovog API-ja dostupan je na [zvaničnoj stranici](https://developers.google.com/youtube/v3/docs/videos/list).

Pre nego li započnemo sa implementacijom našeg prvog REST API-ja korišćenjem Express.js radnog okvira, upoznaćemo i jedan alat koji se koristi za testiranje API-ja.

# Postman

Prilikom razvoja REST API-ja potrebno je testirati da li se dobijaju odgovarajući HTTP odgovori za zadate HTTP zahteve. Kako klasični veb pregledači samo do neke mere podržavaju testiranje GET zahteva, potreban nam je alat koji će nadomestiti ova ograničenja i omogućiti udoban rad.

Naš izbor, a i izbor velikog broja veb zajednice, je alat Postman. Kroz njegov interfejs se može odabrati tip HTTP zahteva, uneti URL adresa resursa, jednostavno podestiti neko od polja HTTP zaglavlja ili njegovo telo u nekoliko ponuđenih formata. Sam HTTP odgovor se može analizirati kroz prikaze statusnog koda i statusne poruke, izlistana polja zaglavlja i telo odgovora. Uz sve ovo alat omogućava i čuvanje i organizaciju korišćenih testova, kao i njihovu automatizaciju.

![slika Postman interfejsa](./assets/postman.png)

Alat Postman se može instalirati preuzimanjem instalacije sa [zvanične stranice](https://www.postman.com/downloads/).

# Instalacija Express.js paketa i Hello World program

Express.js paket se instalira pomoću alata `npm` komandom `npm install express --save`. Dodatna opcija `save` omogućava da se informacije o instaliranoj verziji upišu u `package.json` fajl tekućeg projekta. Trenutno je aktivna verzija 4 ovog okruženja, dok se verzija 5 nalazi u _alfa_ fazi.

Nakon uspešne instalacije, možemo napisati i osnovni _Hello world_ program.

```javascript
const express = require('express');

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
```

Baš kao i drugi Node.js paketi, paket Express.js se učitava funkcijom `require`. Povratna vrednost ovog poziva je funkcija koja se ustaljeno imenuje `express` i čijim se pozivom kreira objekat, u našem slučaju `app` koji predstavlja Express.js aplikaciju. Raspoloživim svojstvima i metodama se može uticati na mnoga ponašanja aplikacije i o tome će svakako biti reči u nastavku. Aplikacija _Hello World_ koristi dve metode, `get()` i `listen()`.

Metodom `listen()` aplikaciju aktiviramo na adresi `http://localhost` i portu `3000` uz prikazivanje poruke u terminalu navedenom funkcijom sa povratnim pozivom.

Metodom `get()` navodimo da aplikacija podržava `GET` zahtev određen putanjom '/' koja je navedena kao prvi argument. Drugi argument ove metode je funkcija sa povratnim pozivom koja definiše obradu ovakvog jednog zahteva. Njeni argumenti `req` i `res` redom predstavljaju primljeni HTTP zahtev i HTTP odgovor koji aplikacija treba da pošalje. Objekat `req` raspolaže svojstvima kojima se mogu ispitati zahtevi. Tako, na primer, svojstvo `method` se može iskoristiti za očitavanje metode, svojstvo `url` za očitavanje putanje zahteva, a svojstvo `body` za očitavanje tela zahteva. Sva ova svojstva radni okvir Express.js implicitno koristi za realizaciju uparivanja pristiglih zahteva sa putanjama koje programski definišu. Slična svojstva se mogu postaviti na nivou `req` objekta istoimenim funkcijama. O njima će svakako biti reči u nastavku. Funkcijom `send` se postavlja telo HTTP odgovora i šalje sam odgovor. Ukoliko joj se kao argument prosledi string, ova funkcija ga automatski upisuje u telo odgovora, postavlja polja zaglavlja `Content-Type` na `text/html` i `Content-Length` na odgovarajuću dužinu sadržaja. Ukoliko joj se prosledi pak niz vrednosti, podrazumevano se vrši transformacija u JSON format i postavlja se polje zaglavlja `Content-Type` na vrednost `application/json`. U našem primeru telo će sadržati poruku `Hello World!`.

Ukoliko ovaj kod sačuvamo, u Postman alatu će se po unosu metode GET i adrese http://localhost:3000/ nakon klika na dugme za slanje pojaviti poruka 'Hello World!'. Ukoliko ovu adresu promenimo, na primer na http://localhost:3000/test.html, Postman će nas obavestiti porukom `'Cannot GET /test.html'` i statusnim kodom `404` da nije u mogućnosti da prikaže ovu stranicu.

U Express.js radnom okviru je običajeno je da se URL adrese tj. putanje koje aplikacija podržava nazivaju `rutama` pa ćemo se nadalje pridržavati te terminologije.

# Razvoj API-ja

API na kojem ćemo raditi treba da omogući manipulaciju nad podacima o korisnicima. Želimo da omogućimo pregled informacija o korisnicima, registrovanje novih korisnika, izmenu podataka postojećih, kao i njihovo brisanje. Korisnike ćemo opisivati jedinstvenim identifikatorom, korisničkim imenom, elektronskom adresom, šifrom i statusom.

```javascript
const user = {
  id: 1,
  username: 'john',
  email: 'john@matf.bg.ac.rs',
  password: 'john123',
  status: 'active',
};
```

U opštem slučaju ove informacije se na nivou aplikacije čuvaju u bazi podataka. Jednostavnosti radi, u ovom primeru ćemo informacije o svim korisnicima čuvati u na nivou modula sa imenom _users.js_ čiji je sadržaj naveden ispod.

```javascript
const users = [
  {
    id: 1,
    username: 'john',
    email: 'john@matf.bg.ac.rs',
    password: 'john123',
    status: 'active',
  },

  {
    id: 2,
    username: 'pavle',
    email: 'pavle@gmail.com',
    password: 'pavle123',
    status: 'active',
  },

  {
    id: 3,
    username: 'maja',
    email: 'maja@gmail.com',
    password: 'maja123',
    status: 'inactive',
  },

  {
    id: 4,
    username: 'klara',
    email: 'klara@gmail.com',
    password: 'klara123',
    status: 'active',
  },
];

module.exports = users;
```

Rute koje će biti podržane API-jem i semantika zahteva nad njima sumirane su u tabeli. Neka dogovor bude i da se za formatiranje  informacija koje klijent i server razmenjuju koristi JSON format.

| HTTP metod        | Ruta           | Značenje  |
| :-------------    |:---------------| :---------------|
| GET               | http://localhost/api/users | čitanje informacije o svim korisnicima |
| GET               | http://localhost/api/users/user-id |   čitanje informacija o korisniku navedenom sa zadatim id-jem |
| POST              | http://localhost/api/users     |    dodavanje novog korisnika |
| PUT               | http://localhost/api/users/user-id  |    ažuriranje šifre korisnika sa zadatim id-jem |
| DELETE            | http://localhost/api/users/user-id  |    brisanje korisnika sa zadatim id-jem |


Prvo ćemo pokriti ponašanje aplikacije u slučaju GET zahteva ka ruti http://localhost/api/users.
```javascript
const express = require('express');
const users = require('./users.js');

const app = express();
const port = 3000;

app.get('/api/users', (req, res) => {
  res.status(200);
  res.set('Content-Type', 'application/json');
  res.send(JSON.stringify(users));
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
```

Nakon učitavanja Express.js modula i modula sa podacima, aplikaciju ćemo kreirati pozivom funkcije `express`. Kako će aplikacija biti aktivna na adresi http://localhost:3000, da bismo obradili GET zahtev nad navedenom rutom pozvaćemo funkciju `get` i proslediti joj putanju `/api/users`. U funkciji sa povratnim pozivom za čije argumente `req` i `res` smo rekli da predstavljaju HTTP zahtev i HTTP odgovor same aplikacije, prvo ćemo iskoristiti metodu `status` objekta `res` da bismo postavili statusni kod odgovora, a zatim i metodu `set` za postavljanje polja zaglavlja odgovora. Metoda `set` očekuje ime polja zaglavlja i njegovu vrednost. Polje zaglavlja `Content-Type` opisuje tip sadržaja pa ćemo je, po dogovoru, postaviti na vrednost `application/json` kako bi klijentu ukazali da će  podaci koji se šalju biti u JSON formatu. Zahtevom treba isporučiti informacije o svim korisnicima, pa je poslednji korak poziv metode `send`. Njoj ćemo proslediti niz sa podacima uz prethodnu transformaciju u JSON format korišćenjem funkcije `JSON.stringify`.

U veb aplikacijama JSON format je čest format razmene podataka pa na nivou radnog okvira imamo na raspolaganju i  funkciju `json`. Njome se podrazumevano prosleđeni sadržaj transformiše u JSON nisku, a polje zaglavlja `Content-Type` postavlja na vrednost `application/json`. Tako bi   
```javascript
res.status(200);
res.json(users);
```
bila skraćena verzija prethodnog koda. Česta praksa je i da se pozivi ovih funkcija ulančavaju pa se umesto dvaju navedenih poziva može napisati i `res.status(200).json(users)`.

Dalje ćemo pokriti ponašanje aplikacije u slučaju GET zahteva ka rutama oblika http://localhost/api/users/user-id. Ovi zahtevi bi trebali, ukoliko se prosledi korektan identifikator korisnika, da dohvate i u JSON formatu vrate klijentu informacije o korisniku sa navedenim identifikatorom.

Prethodnom kodu dodaćemo još jedan poziv metode `get`. Identifikator korisnika na nivou rute je promenljiv pa ćemo prilikom zadavanja putanje ovo ponašanje naglasiti korišćenjem imenovanog parametra `id` rute ispred kojeg ćemo navesti dvotačku. Svi promenljivi delovi ruta se čuvaju na nivou `req.params` objekta i može im se pristupiti preko pridruženih imena. Na primer, za rutu http://localhost:5000/api/blog/:year/:month segmenitma koji predstavljaju godinu i mesec se može pristupiti preko `req.params` objekta sa `req.params.year` i `req.params.month`. Podrazumevani tip pročitanih parametara je string.

```javascript
app.get('/api/users/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const user = users.filter((user) => user.id === parseInt(req.params.id));
    res.status(200);
    res.json(user);
  } else {
    res.status(404);
    res.send();
  }
});
```

Pozivom metode `some` nad nizom korisnika `users` prvo ćemo proveriti da li postoji korisnik sa zadatom vrednošću identifikatora. Ukoliko je odgovor potvrdan, pronaći ćemo objekat pridružen ovom korisniku funkcijom `filter`. U obema funkcijama pretrage smo nakon očitavanja vrednosti parametra `id` koristli funkciju `parseInt` da bi dobili odgovarajuću numeričku vrednost.

Ukoliko korisnik sa zadatim identifikatorom ne postoji, praksa je da se vrati statusni kod `404` kojem odgovara značenje da resurs nije pronađen. Opciono, u ovom slučaju korisniku možemo proslediti i opis greške, na primer, navođenjem objekta koji predstavlja grešku u telu funkcije `send`.

```javascript
res.status(404).send({ error: `Ne postoji korisnik sa identifikatorom ${req.params.id}!` });
```

Ukoliko korisnik za zadatim identifikatorom postoji, vratićemo statusni kod `200` kao indikaciju uspeha i u telu odgovora podatke o pronađenom korisniku u JSON formatu.


Dalje ćemo obraditi POST zahtev korisnika ka ruti http://localhost/api/users i podržati funkcionalnost kreiranja novog korisnika. 

Za obradu zahteva tipa POST iskoristićemo metodu `post` aplikacije. Ova metoda očekuje iste parametre kao i metoda `send`, putanju i funkciju sa povratnim pozivom potpisa `(req, res) => { ...}` koja definiše obradu. Stoga ćemo kao prvi argument proslediti putanju `/api/users`. Da bi kreirali novog korisnika treba da pročitamo podatke iz tela HTTP zahteva koje klijent šalje. Ovi podaci se u praksi mogu prikupiti preko formulara koje korisnici popunjavaju ili dobiti programski na neki drugi način obradom raspoloživih podataka. Telo HTTP zahteva se može pročitati svojstvom `body` na nivou objekta `req` i u opštem slučaju predstavlja čisto tekstualni sadržaj. Ipak, često nam je potrebno da se ovako pročitan sadržaj iz JSON formata ili url-encoded formata transformiše u strukturu koja je podesna za dalju obradu. Ove zadatke je moguće izvesti programski, blokovima JavaScript koda, ali je zbog repeticije bolje iskoristiti podršku Express.js radnog okvira. Zato ćemo uvest modul koji se zove `body-parser` i koji omogućava parsiranje tela zahteva na nekoliko najstandardnijih načina. Paket `body-parser` ćemo prvo instalirati komandom `npm install body-parser`, a potom i uključiti u fajl koji predstavlja naš API.

```javascript
const {urlencoded, json} = require('body-parser');

app.use(json());
app.use(urlencoded({extended: false}));
```
Prilikom uključivanja `body-parser` modula izdvojili smo posebno `json` i `urlencoded` funkcije koje pokrivaju, redom, rad sa JSON i `x-www-form-urlencoded` formatom. Podsetimo se da `x-www-form-urlencoded` format predstavlja zapis podataka u formatu *ključ=vrednost* međusobno spojenih karakterom `&`, na primer, `username=ana&email=ana@gmail.com&password=1234&astatus=active`.  Funkcijom `app.use()` naglašavamo da ove dve funkcije treba primeniti nad svakim HTTP zahtevom koji pristigne do aplikacije. U nastavku ćemo videti da je ovo standardni način zadavanja funkcija srednjeg sloja nivoa aplikacija koji omogućava da se zahtevi i odgovori pripreme serijom obrada pre samog slanja odgovora klijentu. Važno je naglasiti da pozive ovih funkcija treba navesti pre obrada ruta pozivima funkcija `get` i `post`.

```javascript
app.post('/api/users/', (req, res) => {
  if (!req.body.username || !req.body.password || !req.body.email) {
    res.status(400);
    res.send();
  } else {
    const newUser = {
      id: 23,
      username: req.body.username,
      email: req.body.email,
      password: req.body.password,
      status: 'active',
    };

    users.push(newUser);

    res.status(201).json(newUser);
  }
});
```
Zbog korišćenja funkcije `json()` u obradi zahteva sada smo sigurni da prosleđene podatke možemo pročitati preko svojstava `req.body` objekata. Prvo ćemo proveriti da li su prosleđeni svi neophodni podaci za kreiranje novog korisnika. Ukoliko neki od podataka nedostaje, vratićemo odgovor sa statusnim kodom `400` sa značenjem lošeg zahteva. U suprotonom ćemo podatke iskoristiti za kreiranje novog korisnika i ažurirati naš niz sa podacima pozivom funkcije `push` kojom se novokreirani objekat postavlja na kraj niza. Klijentu ćemo vratiti odgovor sa statusnim kodom `201` sa značenjem *kreiran* (engl. created) i opisom novog korisnika. 


Zahtev PUT ka ruti http://localhost/api/users/user-id obradićemo na sličan način korišćenjem metode aplikacije `put`.

```javascript
app.put('/api/users/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    const updatePasswordInfo = req.body;

    users.forEach((user) => {
      if (user.id == parseInt(req.params.id)) {
        if (updatePasswordInfo.currentPassword != user.password) {
          res.status(400).send();
        } else {
          user.password = updatePasswordInfo.newPassword;
          res.status(200).send();
        }
      }
    });
  } else {
    res.status(404).send();
  }
});
```
Prvo ćemo proveriti da li korisnik sa prosleđenim identifikatorom postoji. Identifikator korisnika ćemo očitati preko `req.params.id` svojstva. Ako to nije slučaj vratićemo odgovor sa statusnim kodom `404`. U suprotnom, pročitaćemo podatke iz tela zahteva svojstvom `req.body`. Očekujemo da u telu postoji trenutna šifra korisnika i nova šifra korisnika. Ukoliko se trenutna šifra korisnika ne poklapa sa prosleđenom trenutnom šifrom, nećemo dozvoliti ažuriranje šifre i vratićemo odgovor sa statusnim kodom `400`. U suprotnom ćemo ažurirati šifru i vratiti statusni kod 200 kao indikaciju uspeha. 


Ostalo je još da pokrijemo DELETE zahtev ka ruti  http://localhost/api/users/user-id. Prvo ćemo proveriti da li korisnik sa zadatim identifikatorom postoji. Ako to nije slučaj vratićemo odgovor sa statusnim kodom `404`. U suprotnom ćemo identifikovati ovog korisnika u nizu sa korisnicima i obrisati ga pozivom funkcije `splice`. Korisniku ćemo u tom slučaju vratiti odgovor sa statusnim kodom `200`. 

```javascript
app.delete('/api/users/:id', (req, res) => {
  const isFound = users.some((user) => user.id === parseInt(req.params.id));

  if (isFound) {
    let deleteUserIndex;
    users.forEach((user, index) => {
      if (user.id === parseInt(req.params.id)){
        deleteUserIndex = index;
        return;
      }
    });
    users.splice(deleteUserIndex, 1);
    res.status(200).send();
  } else {
    res.status(404).send();
  }
});
```

U praksi bi se pre operacija dodavanja, izmene i brisanja proverilo i da li postoji autorizacija da se ovakva radnja obavi, na primer, proverom JWT tokena uz podršku paketa [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken).

## Rutiranje

API koji smo razvili je sasvim funkcionalan, ali je sa strane organizacije koda pomalo nepraktičan i nepregledan. U opštem slučaju API-ji su kompleksniji, pokrivaju veći broj entiteta i radnji nad njima pa je nezahvalno sve funkcije čuvati na nivou jednog fajla i upravljati njima preko objekta aplikacije. Radni okvir Express.js podržava i formalni koncept rutera kojim se definiše kako aplikacija reaguje na određene zahteve klijenata ka određenim putanjama. Tako različiti ruteri mogu pokrivati različite aspekte API-ja. Na primer, može postojati poseban ruter koji pokriva zahteve ka putanjama koje počinju sa /api/users i funkcionalnosti nad korisnicima i poseban ruter koji pokriva zahteve ka putanjama koje počinju sa `/api/products` i funkcionalnostima nad proizvodima.  Ruteri se formalno kreiraju pozivom funkcije `Router`.  
```javascript
const express = require('express');
const router = express.Router();
```     

Ruteri raspolažu funkcijama `get`, `post`, `put` i `delete` koje, baš kao i istoimene funkcije korišćene na nivou aplikacije, očekuju putanju i funkciju sa povratnim pozivom koja obrađuje zahtev i generiše odgovor za klijenta. Na primer, sledećim fragmentom koda se definiše akcija za GET zahtev ka resursu /api/users kojom se dobijaju informacije o svim korisnicima. 

```javascript
router.get('/api/users', (req, res)=>{
  res.status(200).json(users);
});
```     
Zato ćemo prethodni kod reorganizovati tako da koristi ove funkcionalnosti, a samu aplikaciju učiniti modularnijom. Pre svega, kreiraćemo poseban direktorijum sa imenom `routes` i u njemu poddirektorijum sa imenom `api`. Dalje u ovom direktorijum ćemo kreirati fajl sa imenom `users.js` sa idejom da odgovara ruteru koji će opsluživati putanje koje počinju sa /api/users. Primetimo da organizacija direktorijuma `routes` prati ugnjžđavanje putanja na nivou API-ja. 

U fajlu `users.js` prvo ćemo učitati modul sa podacima, a potom i kreirati ruter i pridružiti mu akcije. Akcije na nivou rutera ćemo sada zadavati relativno u odnosu na putanju `/api/users/` pa će tako, na primer, prethodnom pozivu `app.get('/api/users', (req, res)=>{...})` odgovarati poziv `router.get('/api/users', (req, res)=>{...})`, a pozivu `app.get('/api/users/:id', (req, res)=>{...})` poziv `router.get('/api/users/:id', (req, res)=>{...})`. Kodovi koje ove funkcije treba da sadrže će biti istovetni kodovima koje smo do sada implementirali. Da bi ruter bilo moguće koristiti, ostaje još izvesti ga na nivou modula naredbom `module.exports=router;`. Sledeći kod sadrži kostur opisanog rutera.

```javascript
const users = require('../../users.js');

const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  // kod za obradu 
});

router.get('/:id', (req, res) => {
  // kod za obradu 
});

router.post('/', (req, res) => {
  // kod za obradu 
});

router.put('/:id', (req, res) => {
  // kod za obradu 
});

router.delete('/:id', (req, res) => {
  // kod za obradu 
});

module.exports = router;
```
Na nivou aplikacije je potrebno uvesti sve rutere koji se koriste. Samo uvoženje rutera se realizuje funkcijom `require` uz zadavanje odgovarajuće putanje rutera, a njegovo pridruživanje aplikaciji postiže se korišćenjem funkcije `use`. Funkciji `use` kao prvi argument ćemo pridružiti prefiks ruta `/api/users` koje naš ruter pokriva. Tako ća aplikacija bez obzira na tip zahteva uporediti putanju zahteva i putanju rutera i ako se njihovi prefiksi poklope dalju obradu proslediti ruteru.  

```javascript
const usersRoutes = require('./routes/api/users');
app.use('/api/users', usersRoutes);
```

Sada ceo kod koji predstavlja aplikaciju sadrži samo podešavanja aplikacije i može modularno da se nadgrađuje.
```javascript
const express = require('express');
const {urlencoded, json} = require('body-parser');

const app = express();
const port = 3000;

app.use(json());
app.use(urlencoded({extended: false}));

const usersRoutes = require('./routes/api/users');
app.use('/api/users', usersRoutes);

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
```

Zanimljivo je razmotriti kako će ruter reagovati ukoliko mu se prosledi zahtev koji počinje prefiksom `/api/users` ali koji nije pokriven logikom rutiranja ili ako mu se prosledi zahtev čiji tip nije pokriven. Na primer, ako se prosledi GET zahtev ka ruti `/api/users/3/status` ona se neće uklopiti ni u jedan od opisanih šablona. Takođe, ako se prosledi zahtev tip `OPTIONS` ka nekoj od podržanih ruta, neće postojati odgovarajuća akcija. Jedan način nadgradnje ovakvog ponašanje je korišćenje regularnih izraza za opisivanje ruta kojima se mogu ignorisati nepotrebna proširenja. Na primer, zadavanjem putanje u obliku `/:id/?.*` biće pokrivena i navedena problematična ruta. Ukoliko ovo nije smisleno na nivou same aplikacije može se, u duhu REST arhitekture, vratiti statusni kod iz kategorije kodova većih ili jednakih od `500` i odgovarajući opis greške. Sledeći fragment koda to i ilustruje. 

```javascript
router.use((req, res, next)=>{
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': `Nepoznat zahtev ${unknownMethod} ka ${unknownPath}!`});
});
```
Ruteru se metodom `use` pridružuje funkcija koja ima potpis nalik do sada diskutovanim. U našem slučaju funkcija očitava metod i putanju samog zahteva i na osnovu nje formira odgovarajuću poruku. Smisao argumenta `next` funkcije sa povratnim pozivom približava sledeća sekcija. 

# Funkcije srednjeg sloja
Srednji sloj (engl. middleware) predstavlja stek funkcija u kojem svaka funkcija ima pristup HTTP zahtevu predstavljenim objektom `req`, HTTP odgovoru predstavljenim objektom `res` i funkciji `next` kojom se prepušta izvršavanje narednoj funkciji steka. Funkcije srednjeg sloja mogu izvršavati proizvoljan kod, menjati zahteve i odgovore, pozvati sledeću funkciju steka ili prekinuti izvršavanje. Stoga se o njima može govoriti na više nivoa. Standardna klasifikacija je na funkcije srednjeg sloja na nivou aplikacije, funkcije srednjeg sloja na nivou rutera, ugrađene funkcije, funkcije nivoa softvera od strane trećih lica (engl. third-party middleware) i funkcije nivo grešaka. 

![srednji sloj aplikacije](./assets/middleware.png)

Mi smo do sada imali prilike da iskoristimo funkciju `body-parse` srednjeg sloja nivoa softvera od strane trećih lica koja je za nas pripremala telo zahteva tako da se čitanja mogu jednostavnije realizovati. Upoznali smo i funkciju na nivou rutera za obradu grešaka koja nam je omogućila reagovanje na aktivnosti koje nisu podržane. Sledeći primer ilustruje korišćenje funkcije srednjeg sloja niva aplikacije. 

Kao što samo ime kaže, funkcije srednjeg sloja nivoa aplikacije se pridružuju samoj Express.js aplikaciji. Za pridruživanje se  koristi funkcija `use` ili neke od `get`, `post` i sličnih metoda čija imena odgovaraju imenima HTTP metoda.  

```javascript
const app = express();
const port = 3000;

const logger = (req, res, next) => {
  console.log('Zahtev je generisan!');
  next();
};

app.use(logger);

app.get('/api/users', (req, res) => {
  res.status(200).json(users);
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
```
Funkcija `logger` je funkcija srednjeg sloja. Ona raspolaže parametrima `req`, `res` i `next` i na nivou konzole ispisuju poruku "Zahtev je generisan!". Sama funkcija je pridružena aplikaciji korišćenjem funkcije `use` bez eksplicitnog navođenja rute za koju se veže što znači da će svaki put, bez obzira na tip zahteva koji pristigne i njegovu putanju, izvršiti. Ukoliko u pregledaču ili Postman alatu unesemo adresu `http://localhost:300/api/users` pre nego li nam se ispišu informacije o korisnicima u terminalu ćemo primetiti poruku koju ispisuje `logger` funkcija. Ukoliko se, na primer, na nivou funkcije `logger` obriše poziv funkcije `next()` u terminalu će biti ispisana poruka, ali zbog prekinutog prenosa izvršavanja sledećoj funkciji srednjeg sloja, onoj koja ispisuje informacije o korisnicima, neće biti ispisa rezultata.  

O ostalim nivoima funkcija srednjeg sloja će biti još reči u nastavku. 

# Isporučivanje statičkih sadržaja

Objekat koji predstavlja HTTP odgovor raspolaže uz metode `send` i `json` metodom `sendFile`. Ova metoda otvara i čita fajl čija se putanja navodi kao argument, a potom i šalje klijentu pročitani sadržaj. Sledeći primer ilustruje isporučivanje fajla sa imenom _index.html_ koji se nalazi u _public_ direktorijumu.

```javascript
const express = require('express');
const path = require('path');

const app = express();
const port = 4000;

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.listen(port, () => {
  console.log(`Aplikacija je aktivna na adresi http://localhost:${port}`);
});
```

Ukoliko fajlova čiji direktan sadržaj treba isporučiti ima više, što je u realnim scenarijima vrlo verovatno, navođenje svih putanja na nivou aplikacije i putanja samih fajlova može biti nezahvalno za održavanje. Zato radni okvir Express.js kroz ugrađenu funkciju srednjeg sloja `express.static` omogućava upravljanje ovakvim statičkim sadržajima. Sama funkcija očekuje kao argument putanju do direktorijuma u kojem se nalaze statički sadržaji. Po pravilu se pridružuje aplikaciji pre pridruživanja drugih funkcija srednjeg sloja za rutiranje kako bi se za zahtevane putanje prvo proverili. U narednom primeru statički sadržaji se nalaze u direktorijumu _public_. Naredni primer sadrži i funkciju srednjeg sloja koja u slučajevima kada se zahteva fajl koji ne pripada kolekciji statičkih sadržaja prikazuje stranu 404.html uz statusni kod 404.

```javascript
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  res.status(404).sendFile(path.join(__dirname, '404.html'));
});
```

# Šabloni i mašine sa rad sa šablonima

U razvoju veb aplikacija česte su i situacije u kojima je na zahteve klijenata potrebno dinamički kreirati sadržaje. Na primer, ukoliko je za svakog korisnika aplikacije potrebno podržati stranicu sa informacijama o profilu, kreiranje statičkih sadržaja nije dobar izbor, kako zbog memorijskih zahteva, tako i zbog neugodnog održavanja i smanjene skalabilnosti. Više bi nam odgovaralo da ove stranice dele neke zajedničke informacije i dizajnerske elemente, a da se samo segmenti stranica koje se odnose na specifične korisničke informacije dinamički menjaju. Ovakvo ponašanje je moguće ostvariti uz korišćenje šablona i mašina za rad sa šablonima.

Da bi kreirali šablone, potreban nam je mehanizam kojim možemo na nivou stranica obeležiti delove koji su statični i delove koji se menjaju. Takođe, potreban nam je i mehanizam koji za kreirani šablon i poznate podatke vrši odgovarajuće zamene i generiše sadržaj koji je potpun i koji se može isporučiti klijentu. Obe ove funkcionalnosti su na nivou radnog okvira Express.js podržane mašinama za rad sa šablonima (engl. template engine). Neke od popularni mašina za rad sa šablonima su [pug](https://www.npmjs.com/package/pug), [mustache](https://www.npmjs.com/package/mustache-express) i [ejs](https://www.npmjs.com/package/ejs). Mi ćemo u radu koristiti `ejs` mašinu.

Mašina za rad sa šablonima `ejs` (akronim od _Embedded JavaScript templates_) omogućava obeležavanje dinamičkih sadržaja stranica na način koji je po sintaksi vrlo sličan jeziku JavaScript. Da bi mogli da je koristimo, moramo je instalirati naredbom `npm install ejs`. Možemo kreirati i direktorijum sa imenom `views` u tekućem radnom direktorijumu u okviru kojeg ćemo čuvati sve šablone. Njih ćemo prepoznavati po ekstenziji `.ejs`.

Pre nego li uvedemo primere kroz koje ćemo upoznati *ejx* šablone, funkcijom `set` na nivou aplikacije postavimo informacije o mašini za rad sa šablonima koju ćemo koristiti i direktorijumu u kojem će se nalaziti šabloni. Sve putanje šablona dalje možemo evaluirati u odnosu na postavljeni direktorijum. Ekstenzija `.ejs` u imenima šablona će se podrazumevati pa ju nije neophodno navoditi.

```javascript
app.set('view engine', 'ejs');
app.set('views', 'views/');
```

Naš prvi zadatak će biti da za rutu oblika http://localhost:3000/users/user-id dinamički, na osnovu vrednosti identifikatora korisnika, prikažemo informacije o korisniku. Pridruživanje rute aplikaciji metodom get, zadatavanje rute i funkcije sa povratnim pozivom odgovara zadacima koje smo do sada razmatrali pa ćemo se usredsrediti na generisanje samog odgovora. Funkcijom `filter`, pre svega, proveravamo da li postoji korisnik sa zadatom vrednošću identifikatora. Ukoliko takav korisnik ne postoji, funkcija `filter` kao rezultat vraća prazan niz, dok u slučaju kada takav korisnik postoji vraća niz sa samo jednim elementom (jer je identifikator jedinstven). Zato promenljiva `user` može imati vrednost *undefined* ili sadržati objekat sa korisničkim podacima.

Metoda `render` na nivou `res` objekta vrši obradu šablona. Ova metoda očekuje dva argumenta, putanju do šablona i objekat sa konkretnim vrednostima koje treba interpolirati u šablon. Ovako dobijen tekstualni sadržaj u HTML formatu metoda dalje šalje klijentu. Opciono, ovoj metodi se može proslediti i treći argument, funkcija potpisa `(err, html) => {....}` koja daje više slobode u obradi potencijalnih grešaka ili analizi pročitanog HTML sadržaja. Ukoliko se navede, odgovor dobijem obradom šablona se mora esplicitno poslati pozivom metode `send`. Podrazumevani način reagovanja na greške je poziv `next(err)` kojim se greška dalje propagira.  

U našem slučaju u šablonu sa imenom `user.ejs` biće na odgovarajućim mestima zamenjene generičke vrednosti vrednostima objekta `user`.

```javascript
app.set('view engine', 'ejs');
app.set('views', 'views/');

app.get('/users/:id', (req, res) => {
  const usersWithId = users.filter(
    (user) => user.id == parseInt(req.params.id)
  );
  const user = usersWithId.length == 0 ? undefined : usersWithId[0];

  res.render('user.ejs', { user: user });
});
```
Sam šablon se nalazi na adresi `views/user.ejs` i predstavlja kombinaciju HTML sadržaja i EJS anotacija. Fragment koda ispod sadrži telo šablona.  
```html
<body>
    <% if (user == undefined ) { %>
    <div class="alert alert-info text-center" role="alert">
      Unknown user!
    </div>
    <%} else {%>
    <div class="card text-center" style="background-color: orange;">
      <div class="card-header">
        User info
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">Username: <%= user.username %></li>
        <li class="list-group-item">Email: <%= user.email %></li>
        <li class="list-group-item">Status: <%= user.password %></li>
      </ul>
    </div>
    <%}%>
  </body>
```
 Možemo primetiti da na nivou šablona postoje dve vrste anotacija. Anotacije obeležene sa `<%=` i `%>` predstavljaju vrednosti koje će biti izračunate kao JavaScript izrazi i dalje transformisane u niske. Anotacije obeležene sa `<%` i `%>` predstavljaju JavaScript kontrolne konstrukcije poput grananja i petlji. Tako će, ukoliko vrednost prosleđene promenljive *user* bude *undefined*, biti prikazana poruka da korisnik sa navedenim identifikatorom ne postoji. U suprotnom će biti prikazana lista sa podacima korisnika kao na slici.   

![prikaz za adresu http://localhost:3000/users/2](./assets/ejs_users_id.png)


Drugi zadatak će biti da za rutu oblika http://localhost:3000/users prikažemo informacije o svim korisnicima. Funkcija `render` u ovom slučaju prima putanju do šablona `users.ejs` i objekat sa svojstvima `title` i `users` koja redom predstavljaju naslov stranice i niz svih korisnika. 

```javascript
app.set('view engine', 'ejs');
app.set('views', 'views/');

app.get('/users', (req, res) => {
  res.render('users.ejs', { title: 'All users', users: users });
});
```
Šablon koji se zbog podešavanja aplikacije nalazi na adresi `views/users.ejs` ovoga puta sadrži klasičnu `forEach` konstrukciju u kojoj se u telu funkcije sa povratnim pozivom, za svakog korisnika pojedinačno, ispisuju tražene informacije. 

```html
<body>
    <h1><%= title %></h1>
    <% users.forEach(user => { %>
    <div class="card" style="width: 25rem; background-color: orange;">
      <div class="card-header">
        User info
      </div>
      <ul class="list-group list-group-flush">
        <li class="list-group-item">Username: <%= user.username %></li>
        <li class="list-group-item">Email: <%= user.email %></li>
        <li class="list-group-item">Password: <%= user.password %></li>
      </ul>
    </div>
    <br />
    <%})%>
  </body>
  ```
Slika ispod odgovara postignutom prikazu za prva dva korisnika. 

![prikaz za adresu http://localhost:3000/users](./assets/ejs_users.png)

