//
console.log(2 ** 3);

//
console.log(2e2 + 1);

//
console.log(34 + undefined);

//
console.log(34 + null);

//
console.log(34 / 'abc');

//
console.log(2 < 3 < 1);

//
console.log(10 < 3 < 1);

//
console.log(2 < 3 + 5);

//
console.log((2 < 3) + 5);

//
let a = 5;
const b = [3, 4, 5];
b[0] = 8;
console.log(a, b);

//
let x = (y = 2);
console.log(`${x} + {y} = ${x + y}`);

//
console.log(10 || undefined);

//
console.log('ana' || 0);

//
console.log(undefined || 5);

//
let dan = 'ponedeljak';
let poruka;

if (dan == 'petak') {
  poruka = 'Juhu, sledi vikend!';
} else {
  poruka = 'Odgovorno radimo.';
}

console.log(poruka);

//
var temperature = 22;
switch (temperature) {
  case 30:
    console.log('Too warm.');
    break;
  case 22:
    console.log('It\'s ok.');
  case 5:
    console.log('Too cold.');
    break;
}

//
let izbor = '5';
let bonus;

switch (izbor) {
  case 1:
    bonus = 100;
    break;
  case 3:
    bonus = 200;
    break;
  case 5:
    bonus = 450;
    break;
  default:
    bonus = 50;
}

console.log(bonus);

//
let i;
for (i = 0; i < 10; i += 2) {
  console.log(i);
}

//
let sum = 0;
for (let i = 0; i < 10; i++) {
  if (i % 3 == 0) {
    sum++;
  }
  if (i == 8) {
    break;
  }
}
console.log(sum);

//
let sum = 0;
for (let i = 0; i < 10; i++) {
  if (i == 6) {
    continue;
  }
  if (i % 3 == 0) {
    sum++;
  }
}
console.log(sum);

//
function sum() {
  return arguments[0] + arguments[2];
}

let result;
result = sum(2, 5, 11, 'abc');
console.log(result);

//
let f = (a, b) => a + b;
console.log(f(2 * 3, 2 - 1));

//
let calculateCapacity = (fileSize, numberOfFiles) => {
  fileSize * numberOfFiles;
};
console.log(calculateCapacity(100, 5));

//
console.log(mul(2, 3));
var mul = function(x, y) {
  return x * y;
};

//
console.log(mul(2, 3));
let mul = function(x, y) {
  return x * y;
};


//
var i = 23; 

function print(n){
  for (i=0; i<=n; i++){
    console.log(i);
  }
}

console.log(n);
console.log(i);


// 
console.log(undefined + 10);

// 
var undefined  = 5;
console.log(undefined + 10);

// 
var b = 5;
function f(a, a, c){
  return a + b + c;
}

console.log(f(1, 2, 3));
