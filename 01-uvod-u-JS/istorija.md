# Kratka i neformalna istorija jezika JavaScript

Jezik JavaScript se pojavio 1995. godine kao integralni deo pregledača Netscape. Veb je u to vreme bio tek u povoju, postojala je nekolicina pregledača sa skromnim funkcionalnostima, a među njima se isticao Netscape. Netscape je bila mala firma koja je pregledač imala kao jedini proizvod, sa besplatnom verzijom samo za nekomercijalne i akademske svrhe. Majkrosoft  je u to vreme razmišljao o uvođenju Internet Explorer pregledača koji bi nudio besplatno u sklopu svog operativnog sistema. Ovakva nategnuta klima je uticala da se mnoge funkcionalnosti u jezik JavaScript dodaju brzo i možda ne sasvim logično, kao i da se ceo proces standardizacije jezika vrlo rano inicira kako bi predupredio monopolističke namere. Poslovni partner Netscape organizaciji je bila kompanija Sun koja je stajala iza jezika Java. Iako dele deo imena, JavaScript i Java nemaju tačaka preseka i predstavljaju dva sasvim različita jezika. Verzija skript jezika JScript koju je u paraleli razvijao Majkrosoft imala je svoje nadgradnje. Zato su se pravile veb strane ciljano namenjene ili jednom ili drugom pregledaču, iz ugla programera prilično komplikovana i nepoželjna situacija. U periodu od 1997. do 1999. godine postojale su 3 verzije ECMA standarda sa ciljem da ove razlike harmonizuju. Verzija 4 je bila kontraverzna i nije bila podržana od mnogih velikih kompanija. Verzija 5 je bila stabilna i postojana verzija (od 2009. do 2015. godine), dok je verzija 6 donela mnoge i značajne novine u sam jezik. Od tada se redovno, gotovo na godišnjem nivou, radi na predlaganju i standardizaciji novih svojstava jezika. 

![](./assets/ES5_ES6_ES7_TypeScript.png)

## Zanimljivosti:

* Autor jezika JavaScript je Brendan Eich. Mogu se pronaći podaci koji kažu da je jezik nastao za svega 10 dana. 

* [TC39](https://github.com/tc39) je organizacija koja se bavi standardizacijom jezika. 

* Prema [StackOverflow statistici](https://insights.stackoverflow.com/survey/2021), JavaScript je načešće korišćeni jezik u 2021. godini (64.96%).

* [Browser wars](https://en.wikipedia.org/wiki/Browser_wars)

## Izvori: 

* https://www.jetbrains.com/lp/javascript-25/ 

* https://www.springboard.com/blog/history-of-javascript/

* https://en.wikipedia.org/wiki/ECMAScript#7th_Edition_-_ECMAScript_2016

