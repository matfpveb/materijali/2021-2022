# 1. sedmica vežbi

## O tehnologijama koje se istražuju na kursu

- [Istorija jezika JavaScript](./istorija.md)
- [MEAN stek](./mean.md)
- [Veb zanimljivosti](./zanimljivosti.md)

## JavaScript

- Sekcije 2.1, 2.2 i 2.3 u [skripti](https://www.nikolaajzenhamer.rs/assets/pdf/pzv.pdf){:target="_blank"}
