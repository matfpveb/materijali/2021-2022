# MEAN stek

Termin *stek* označava skupa tehnologija koje se zajednički koriste u implementaciji veb rešenja. Različite stekove karakterišu različite osobine i scenariji primena, pa u tom smislu, ne postoji univerzalno najbolje rešenje. 

**MEAN** stek predstavlja kombinaciju **MongoDB** baze podataka, **Express.js** radnog okvira za veb, **Angular** radnog okvira i **Node.js** okruženja. Svaka od navedenih tehnologija je bazirana na jeziku JavaScript.  

[MongoDB](https://www.mongodb.com/) je noSQL baza podataka bazirana na dokumentima organizovanih poput JSON fajlova.  Podržava funkcionalnosti poput indeksiranja i agregacije u realnom vremenu, distribuirana je i vrlo skalabilna.

[Express.js](https://expressjs.com/) je najpopularniji veb radni okvir razvijen za Node.js okruženje. Koristi se primarno za razvoj aplikacionih programskih interfejsa (API-ja). Koriste ga kompanije PayPal, Uber, IBM, ... 

[Angular](https://angular.io/) radni okvir dolazi iz kompanije Google i koristi se primarno za razvoj jednostraničnih aplikacija. Zarad lakšeg razvoja i testiranja aplikacija, Angular radni okvir koristi TypeScript.

[Node.js](https://nodejs.org/) je okruženje za izvršavanje JavaScript koda. Zasnovano na V8 JavaScript mašini koja je sastavni deo Chrome pregledača. Node.js se koristi za razvoj mrežnih aplikacija koje nisu previše račnunski zahtevne kako bi neblokirajuća svojstva koja ga karakterišu i nude veliku skalabilnost i brzinu došla do izražaja.  

Podjednako popularni stekovi bazirani u potpunosti na jeziku JavaScript su **MERN** i **MEVN** u kojima se umesto radnog okvira Angular koriste, redom, okviri **React** i **Vue**. Alternativni stek koji koristi jezik JavaScript samo na strani klijenta, a jezike Perl, Python ili PHP za implementaciju  pozadinskog dela, je **LAMP** (Linux-Apache-MySQL-Python/PHP). 

![](./assets/mean_stack.jpg)
