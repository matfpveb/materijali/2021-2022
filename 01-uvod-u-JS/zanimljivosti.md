# Zanimljivosti

### Orijentacija: 
* Road Map, web developer: https://github.com/kamranahmedse/developer-roadmap

### Stilske preporuke (eng. styleguides): 
* https://github.com/airbnb/javascript
* https://google.github.io/styleguide/jsguide.html
* https://standardjs.com/

### JavaScript Scope vizuelizacija
* https://tylermcginnis.com/javascript-visualizer/
