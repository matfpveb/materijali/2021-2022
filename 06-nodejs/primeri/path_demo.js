const path = require('path');

const testPath = '/home/user/dir/plans.txt';

const name = path.basename(testPath);
console.log('Ime fajla: ', name);

const extension = path.extname(testPath);
console.log('Ekstenzija fajla: ', extension);

const parent = path.dirname(testPath);
console.log('Roditeljski direktorijum: ', parent); 

const pathInfo = path.parse(testPath);
console.log('Informacije o fajlu: ', pathInfo);

// kreira se putanja /home/user/dir/programming/learning_js.txt
const learningJSPath = path.join(parent, 'programming', 'learning_js.txt');
console.log(learningJSPath);
