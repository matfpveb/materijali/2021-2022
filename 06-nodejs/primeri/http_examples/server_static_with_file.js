const http = require('http');
const fs = require('fs');
const path = require('path');

const PORT = process.env.PORT || 5000;

const server = http.createServer((req, res) => {
  const pageName = 'index.html';
  const pagePath = path.join(__dirname, 'public', pageName);

  fs.readFile(pagePath, 'utf8', (readError, readData) => {
    if (readError) {
      res.statusCode = 500;
      res.end();
      return;
    }

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.write(readData);
    res.end();
  });
});

server.listen(PORT, () => {
  console.log('Server je pokrenut!');
});
