const http = require('http');

const options = {
  host: 'localhost',
  port: 5000,
  path: '/about.html',
  method: 'GET',
};

const client = http.request(options, (response) => {
  console.log('Statusni kod odgovora servera: ', response.statusCode);

  response.on('data', (data) => {
    process.stdout.write(data);
  });
});

client.on('error', (error) => {
  console.log('Greska: ', error);
});

client.end();
