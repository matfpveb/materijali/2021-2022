const http = require('http');

const PORT = process.env.PORT || 5000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');

  const bodyContent = '<p> Zdravo svima! </p>';
  res.write(bodyContent);
  res.end();
});

server.listen(PORT, () => {
  console.log('Server je pokrenut!');
});
