const http = require('http');
const fs = require('fs');
const path = require('path');

const PORT = process.env.PORT || 5000;

const server = http.createServer((req, res) => {
  let page = req.url.substr(1);

  if (page == '') {
    page = 'index.html';
  }

  console.log(page);

  const pagePath = path.join(__dirname, 'public', page);

  fs.readFile(pagePath, 'utf8', (readError, readData) => {
    if (readError) {
      if (readError.code == 'ENOENT') {
        res.statusCode = 404;
        const file404path = path.join(__dirname, 'public', '404.html');
        fs.readFile(file404path, 'utf8', (read404Error, read404Data) => {
          if (read404Error) {
            throw read404Error;
          }
          res.write(read404Data);
          res.end();
        });
      } else {
        res.statusCode = 500;
        res.statusMessage = 'Server error!';
        res.end();
      }
      return;
    }

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.write(readData);
    res.end();
  });
});

server.listen(PORT, () => {
  console.log('Server je pokrenut!');
});
