const TicketManager = require('./ticketManager');
const EmailService = require('./emailService');
const DatabaseService = require('./databaseService');

const ticketManager = new TicketManager(3);
const emailService = new EmailService();
const databaseService = new DatabaseService();

ticketManager.on('error', (error) => {
  console.error(`Gracefully handling our error: ${error}`);
});

ticketManager.on('buy', (email, price, timestamp) => {
  emailService.send(email);
  databaseService.save(email, price, timestamp);
});

ticketManager.buy('pera@email.com', 20);
ticketManager.buy('ana@email.com', 40);
ticketManager.buy('zika@email.com', 10);
ticketManager.buy('laza@email.com', 10);
