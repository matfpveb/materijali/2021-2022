const fs = require('fs')

const filePathExists = './hello_all.txt'
const filePathNoExists = './by_all.txt'

// provera da li fajl sa zadatom putanjom postoji ili ne na asinhroni nacin
fs.access(filePathNoExists, fs.constants.F_OK, (error) => {
  if (error) {
    console.error(error.message);
    return;
  }

  console.log('File exists!');
});

// promenama konstante na R_OK, W_OK ili X_OK mozemo proveriti da li imamo prava 
// citanja, pisanja ili izvrsavanja odabranog fajla

