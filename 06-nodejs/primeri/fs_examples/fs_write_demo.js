const fs = require('fs');

const filePath = './hello_all.txt';

const content = 'Hello all!';

fs.writeFile(filePath, content, { encoding: 'utf8' }, (writingError) => {
  if (writingError) {
    throw writingError;
  }

  console.log('All content writen!');
});

process.on('uncaughtException', (error) => {
  console.log('Error handling: ', error.message);
  process.exit(1);
});
