const fs = require('fs');

const testFilePath = './hello_all.txt';

fs.readFile(testFilePath, 'utf8', (readingError, fileData) => {
  if (readingError) {
    // doslo je do greske prilikom citanja
    // console.log(readingError.code);
    // console.log(readingError.message);
    // console.log(readingError.errno);
    // return;
    throw readingError;
  }

  console.log(fileData);
});

process.on('uncaughtException', (error) => {
  console.log('Error handling: ', error.message);
  process.exit(1);
});
