const os = require('os');

// informacije o sistemu
console.log(os.platform());

// informacije o memoriji
console.log(os.totalmem());
console.log(os.freemem());

// informacije o arhitekturi
console.log(os.arch());
console.log(os.cpus());
console.log(os.endianness());
