# Node.js

U ovom poglavlju biće predstavljano okruženje za izvršavanje JavaScript kodova (engl. JavaScript runtime environment) koje se zove Node.js. Zahvaljujući ovom okruženju, kodovi napisani na jeziku JavaScript su prvi put dobili mogućnost da se izvršavaju van pregledača i učestvuju u kreiranju serverskih aplikacija. Zato se u literaturi često navodi da sa ovim okruženjem počinje era "JavaScript svuda" (engl. JavaScript everywhere).

## Nastanak okruženja Node.js i njegove primarne osobine

Do 2009. godine kodovi napisani na jeziku JavaScript mogli su da se izvršavaju isključivo u okviru pregledača. Komponenta pregledača koja se zove JavaScript mašina (engl. JavaScript engine) je omogućavala da se JavaScript kodovi integrisani u veb stranice interpretiraju i da se dobijeni rezultati vrate glavnoj komponenti pregledača. Svi popularni pregledači imaju svoje JavaScript mašine. Tako je `Chakra` mašina koju koristi pregledač Edge, `SpiderMonkey` mašina koju koristi pregledač Mozilla Firefox, a `WebKit` mašina koju koristi Safari. Mašina `V8` koju koristi pregledač Google Chrome je iskorišćenja za razvoj okruženja Node.js.

Inicijalna arhitektura Node.js okruženja trebala je da donese olakšice sistemima koji opslužuju veliki broj klijenata. Ovakvi sistemi su za pojedinačne zahteve klijenata kreirali pojedinačne niti koje ih opslužuju pa je skalabilnost bila uslovljena tehničkim mogućnostima i količinom hardvera koji može da podrži porast konkurentnosti. Stoga Node.js karakteriše jednonitni pristup skoncentrisan na asinhrone (neblokirajuće) pozive i programiranje vođeno događajima (engl. event-driven programming).

![Arhitektura Node.js okruženja](assets/node_architecture.png)

Zbog svojih karakteristika, Node.js se koristi za razvoj veb aplikacija u realnom vremenu (npr. programa za ćaskanja), razvoj REST orijentisanih API-ja, mikroservisa, aplikacija sa velikim brojem ulazno-izlaznih operacija, ali i za razvoj pomoćnih komandnih alata. Sa druge strane, zbog mogućeg zauzeća resursa, Node.js nije preporučljivo koristi za razvoj računski zahtevnih aplikacija.

## Instalacija Node.js okruženja i alat _npm_ za upravljanje paketima

Iako je o instalaciji Node.js okruženja već bilo reči, podsetićemo se da je za uspešno korišćenje neophodno preuzeti i pokrenuti instalaciju sa [zvanične adrese](https://nodejs.org/en/zvaničnog) zajednice. Instalacija je dostupna za sve popularne operativne sisteme i arhitekture. Uspešnost instalacije može se proveriti pokretanjem komande `node --version` kojom se ispisuje verzija instalirane Node.js verzije. U vreme pisanja ove skripte, dostupne su LTS verzija 12.16.2 i, dodatno, verzija 13.13.0 na kojoj su napisani priloženi kodovi.

Pokretanjem komande `node` u terminalu aktivira se interaktivni mod u kojem se mogu kucati JavaScript kodovi. Primer interaktivnog korišćenja možemo videti u fragmentu koda ispod.

```
Welcome to Node.js v13.13.0.
Type ".help" for more information.
>1+3
3
>const poruka = 'Zdravo svima!';
undefined
>poruka
'Zdravo svima!'
>.exit
```

Naredbom `.exit` se izlazi iz interaktivnog moda Node.js okruženja.

Iako moguć, ovakav način korišćenja Node.js okruženja u praksi nije naročito čest. Daleko je zastupljeniji pristup u kojem se okruženju prosleđuje putanja do fajla u kojem se nalazi JavaScript kod i, eventualno, argumenti neophodni za njegovo izvršavanje. Tako se, recimo, za fajl `pozdrav.js`

```javascript
const ime = process.argv[2];
const poruka = `Zdravo ${ime}!`;
console.log(poruka);
```

izvršavanjem komande

```
node pozdrav.js svima
```

ispisuje poruka `Pozdrav svima!`. Navođenje ekstenzije `.js` na nivou JavaScript fajlova nije obavezno pa bi komanda ```node pozdrav svima``` dala isti rezultat. 

U ovom malom programu korišćen je globalni Node.js objekat koji se zove `process` i koji referiše na proces zaslužan za izvršavanje samog koda. Njegovo svojstvo `argv` predstavlja niz svih argumenata koji se prosleđuju programu. Baš kao i u drugim programskim jezicima, argumenti se tretiraju kao niske, a elementi na pozicijama `0` i `1` redom predstavljaju komandu `node` i ime programa koji se izvršava (ili njegovu putanju).

Preko objekta `process` i njegovih svojstava `stdin`, `stdout` i `stderr` može se pristupiti strimovima za čitanje, pisanje i izlaz za greške. Funkcija `console.log` koju smo koristitli u kontekstu pregledača predstavlja alijas za funkciju `process.stdout.write` kojom se vrši ispis u terminalu i nadalje ćemo je uredno koristiti.

Node.js u velikoj meri pordržava ES6 verziju jezika JavaScript i prati predloge zajednice za dalja unapređivanja. Nova svojstva se na nivou verzije mogu izlistati komandom `node --v8-options | grep "in progress"`, a uvid u punu podršku se planski mpže pratiti na [sajtu zajednice](https://fhinkel.rocks/six-speed/).

### Alat npm

Uz okruženje Node.js dolazi i alat `npm` (akronim od *Node Package Manager*) koji omogućava rukovanje paketima nephodnim za razvoj aplikacija. Podsetićemo se nekoliko najvažnijih komandi.

Komandom `npm init` se tekući direktorijum inicijalizuje za rad. Inicijalizacija podrazumeva kreiranje `package.json` fajla u kojem se kroz seriju interakcija mogu uneti ime projekta (paketa), njegova verzija, opis, ime početnog fajla projekta (podrazumevano `index.js`), adresa GitHub repozitorijuma za koji se projekat veže, ključne reči, ime autora i licenca. Sve ove informacije se čuvaju u JSON formatu. Primer inicijalne verzije `package.json` fajla možete videti u nastavku.

```json
{
  "name": "demo-projekat",
  "version": "1.0.0",
  "description": "Node.js demo projekat",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "PVEB tim",
  "license": "ISC"
}
```

Komandom `npm install ime-paketa` vrši se instaliranje paketa navedenog imena. Svi paketi se podrazumevano prevlače sa zajedničkog repozitorijuma `npm` zajednice (https://www.npmjs.com/) koja je vrlo brojna i vrlo kreativna. Nakon instalacije prvog paketa u radnom direktorijumu će se automatski kreirati direktorijum `node_modules` u kojem će se naći kod instaliranog paketa. Takođe, automatski će se ažurirati i `package.json` fajl informacijama o instaliranom paketu. Biće dodata sekcija sa imenom `dependences` u kojoj će biti navedeno ime instaliranog paketa i njegova verzija u formatu koji prati semantičko verzionisanje. Pošto se zavisnosti na nivou Node.js projekata čuvaju rekurzivno, nakon inicijalne instalacije primetićemo da se kreirao i `package-lock.json` fajl koji čuva informacije o instaliranom paketu, ali i o svim paketima od kojih on zavisi. Kodovi ovih projekata će se naći i u `node_modules` direktorijumu.

Sve instalacije na nivou jednog projekta su lokalnog karaktera. Ukoliko se prilikom instalacije navede komanda `npm install -g ime-paketa` sa opcijom `-g` paket će biti instaliran globalno i biće dostupan svim projektima iz bilo kog dela sistema.

Glavna prednost ovake organizacije projekata je u tome što je za pokretanje projekta na drugoj mašini dovoljno podeliti kod i `package.json` datoteku. Komandom `npm install` će se instalirati svi paketi navedeni u `package.json` datoteci i kreirati celokupno okruženje za rad. Zato se, po pravilu, direktorijum `node_modules` nikada ne deli.

Osim paketa koji su potrebni za razvoj samih aplikacija, često su nam potrebni i paketi koji mogu da unaprede i ubrzaju razvoj. Pošto ti paketi nisu od krucijalne važnosti za rad same aplikacije, praksa je da se instaliraju komandom `npm install -D ime-dev-paketa` uz navođenje opcije `-D`. Takvi paketi će u `package.json` fajlu biti izdvojeni u zasebnu sekciju koja se zove `devDependencies`. Paket koji pripada ovoj grupi i koji ćemo odmah instalirati i opisati zove se `nodemon`.

Uobičajeni način programiranja je ikrementalan i podrazumava postepeno dodavanja funkcionalnosti i njihovo testiranje. To znači da nakon svake izmene polaznog JavaScript fajla, recimo index.js fajla, moramo izvršiti komandu `node index.js`. Ovaj postupak treba ponoviti nakon svake izmene, bilo male bilo velike, i najčešće podrazumeva prelazak iz editora u konzolu, prekidanje aktivnog procesa i pokretanje novog. Ovi koraci se mogu automatizovati korišćenjem `nodemon` alata. Dovoljno je nakon instalacije JavaScipt kod izvrišiti komandom `nodemon index.js` i nadalje dinamički pratiti izlaze programa. Komandom `control-C` se može prekinu izvršavanje `nodemon` alata.

Napomena: Ako želite da instalirate noviju verziju Node.js okruženja, možete ponoviti proces preuzimanja i instalacije odgovarajuće verzije. Alternativa je korišćenje alata koji se kratko zove `n`, a kojim se mogu pratiti informacije o aktuelnoj instaliranoj verziji, njene nadgradnje ili izvršiti brisanje. Nakon instalacije paketa `n` komandom `npm install -g n`, sva dalja ažuriranja se mogu realizovati komandom `n latest`. U oba slučaja će vrlo verovatno trebati `sudo` privilegije.

## Moduli

Okruženje Node.js u radu sa modulima koristi `CommonJS` konvencije. Stoga se svi moduli, bez obzira da li je reč o korisnički-definisanim modulima ili o sistemskim-ugrađenim modulima, učitavaju korišćenjem `require` funkcije. Između njihovog učitavanja ipak postoji mala sintaksna razlika. Kod ugrađenih modula očekuje se navođenje samog imena modula, na primer, `require('http')`, dok se u slučaju korisnčki-definisanih modula očekuje navođenje referentne putanje, na primer `require('../public/logger.js')`.

Da bismo u potpunosti mogli da iskoristimo funkcionalnosti Node.js modula, upoznaćemo i neke njihove implementacione detalje.

Svaki Node.js fajl je jedan zaseban modul pa sve promenljive i sve funkcije koje se u njemu nalaze imaju opseg važenja samog fajla. Ovakvo ponašanje treba da nadomesti poteškoće koje je priređivao globalni opseg u dosadašnjoj priči o JavaScript jeziku, a koje su se odnosile na otežano otkrivanje grešaka i oraganizaciju samih projekata. Globalni objekat Node.js okruženja koji se zove `global` (ekvialent `window` objekta u pregledačima) je ipak stavljen na raspolaganje i sve promenljive koje se direktno čuvaju na nivou ovog objekta su globalno vidljive. U praksi je preporučljivije da se deljenja svojstava na nivou modula realizuje preko `export` mehanizma.

Pre nego li izvrši kod koji se nalazi u modulu, Node.js okruženje mu pridruži omotač sa sledećim potpisom
```javascript
(function(exports, require, module, __filename, __dirname){
    // kod modula
});
```
Parametri funkcije omotača stavljaju na raspolaganje modulu dodatne informacije o konteksu izvršavanja. Parametar `exports` funkcije omotača je referenca na `module.exports` objekat i ne može biti prezapisana (engl. override), parametar `require` stavlja na raspolaganje funkciju `require` za učitavanje novih modula, dok preko parametara `__filename` i `__dirname` modul raspolaže informacijama o svom imenu i fizičkoj putanji što omogućava lakše lociranje i rad sa fajl sistemom. Mi ćemo se nadalje oslanjati na dostupnost ovih informacija.

### Ugrađeni moduli

Node.js raspolaže kolekcijom ugrađenih modula koji olakšavaju česte zadatke. Mi ćemo upoznati, za početak, najvažnije, a za dalje informacije upućujemo čitaoce na [zvaničnu dokumentaciju](https://nodejs.org/docs/latest-v13.x/api/).

#### Modul path

Manipulacije nad putanjama su podržane modulom koji se zove `path`. Zahvaljujući funkcijama koje objedinjuje iz putanja možemo jednostavno izdvojiti imena fajlova, imena roditeljskih direktorijuma, ekstenzije fajlova i slično. Pogledajmo kako možemo izdvojiti navedene informacije za putanju `/home/user/dir/plans.txt`.

```javascript
const path = require('path');

const testPath = '/home/user/dir/plans.txt';

const name = path.basename(testPath);
console.log('Ime fajla: ', name);

const extension = path.extname(testPath);
console.log('Ekstenzija fajla: ', extension);

const parent = path.dirname(testPath);
console.log('Roditeljski direktorijum: ', parent);

const pathInfo = path.parse(testPath);
console.log('Informacije o fajlu: ', pathInfo);
```

Funkcija `parse` kreira objekat sa svojstvima `root`, `dir`, `base`, `ext` i `name` koja opisuju prosleđenu putanju.

Na nivou modula biće nam zanimljiva i funkcija `join` koja na osnovu prosleđenih parčića kreira odgovarajuću putanju. Na primer, putanju do fajla sa imenom learning_js.txt koji se nalazi u poddirektorijumu sa imenom programming (koji je na istom nivou kao i datoteka plans.txt) se može dobiti sledećim fragmentom koda:

```javascript
const learningJSPath = path.join(parent, 'programming', 'learning_js.txt');
console.log(learningJSPath);
```

Separator u putanji će biti prilagođen operativnom sistemu.

#### Modul os

Modul `os` omugućava očitavanje informacije o sistemu na kojem se izvršava Node.js kod. Zahvaljujući raspoloživim funkcijama možemo izdvojiti ime pltforme (operativnog sistema), kapacitet raspoložive i slobodne memorije, informacije o jezgrima procesora i slično. Sledeći primer ilustruje izdvajanje navedenih informacija.

```javascript
const os = require('os');

// informacije o sistemu
console.log(os.platform());

// informacije o memoriji
console.log(os.totalmem());
console.log(os.freemem());

// informacije o arhitekturi
console.log(os.arch());
console.log(os.cpus());
console.log(os.endianness());
```

#### Modul fs

Funkcije za rad sa fajl sistemom dostupne su preko modula sa imenom `fs`. Pomoću njih, moguće je programski kreirati fajlove i direktorijume, promeniti svojstva postojećih, pročitati i izmeniti njihove sadržaje ili ih obrisati. Ovde je posebno važno naglasiti da za većinu funkcija postoje dve forme, sinhrona i asinhrona. Sinhrone verzije funkcija možemo prepoznati po sufiksu `Sync` u nazivu. One su po svojoj prirodi blokirajuće pa ih pažljivo treba koristiti. Takođe, dobra praksa je ne kombinovati sinhrone i asinhrone pristupe na nivou istih funkcija.

Sledeći primer ilustruje asinhrono čitanje sadržaja fajla sa imenom *node_intro.txt* koji postoji u tekućem direktorijumu.

```javascript
const fs = require('fs');

fs.readFile('node_intro.txt', 'utf8', (err, data) => {
  if (err) {
    throw err;
  }
  console.log('Sadrzaj datoteke: ');
  console.log(data);
});
```

Funkcija `readFile` očekuje ime fajla za čitanje, nisku koja predstavlja kodnu shemu i funkciju sa povratnim pozivom koja će se izvršiti nakon čitanja sadržaja. Ova funkcija kao prvi argument ima objekat `err` tipa `Error` koji predstavlja grešku. Ukoliko je čitanje uspešno izvršeno, vrednost `err` objekta će biti `null`. U suprotnom, `err` objekat će sadržati sve neophodne informacije o grešci. Drugi argument funkcije je takozvani `data` objekat koji predstavlja, u slučaju uspešnog čitanja, pročitane podatke. U napisanoj funkciji zato prvo proveravamo uspešnost čitanja, a potom ispisujemo podatke.

Ukoliko datoteka `node_intro.txt` ne postoji u radnom direktorijumu, prijaviće nam se greška sa opisom `[Error: ENOENT: no such file or directory, open 'node_intro.txt']`. Nju je moguće generisati na osnovu koda greške i podataka koji su raspoloživi na nivou `err` objekta. U radu ćemo zbog čitljivosti programa pratiti imena grešaka pridružena odgovarajućim kodovima. Njih ćemo očitavati preko `err.code` svojstva, a biće nam zanimljive sledeće vrednosti:

- ENOENT sa značenjem 'No such file or directory'
- EACCES sa značenjem 'Permission denied'
- EEXIST sa značenjem 'File exists'
- EISDIR sa značenjem 'Is a directory'

Detaljna lista grešaka i njihovih opisa se može pronaći u [zvaničnoj dokumentaciji](https://nodejs.org/api/errors.html).

Napomenimo još da se standardni `tray-catch` mehanizam **ne može** koristiti za obradu prijavljenih grešaka u asinhronim pozivima iz prostog razloga što će u trenutku izvršenja funkcije sa povratnim pozivom polazni kod već biti izvršen.

Ovaj pristup prijavljivanja grešaka karakteriše sve asinhrone funkcije i predstavlja takozvani "prvo greška" obrazac (engl. error-first callback pattern).

Zanimljivo je napomenu da ukoliko se ne navede kodna shema sadržaja prilikom čitanja, sadržaj se ne tretira kao tekst već kao binarni bafer. Za predstavljanje bafera se koristi objekat `Buffer`, struktura nalik nizu koja sadrži numeričke reprezentacije sadržaja dužine 8 bita. Tako će sledeći kod ispisati broj bajtova sadržaja (16), kao i kod prvog pročitanog bajta (broj 78 koji odgovara ASCII kodu početnog slova N). 

```javascript
fs.readFile('node_intro.txt', (err, buffer) => {
  if (err) {
    throw err;
  }
  console.log('Duzina sadrzaja datoteke: ', buffer.length);
  console.log('Prvi bajt sadrzaja: ', buffer[0]);
});
```

Pogledajmo sada kako možemo kreirati fajl sa imenom *node_description.txt* i u njemu upisati poruku "Node.js je super!".

```javascript
const fs = require('fs');

const content = 'Node.js je super!';

fs.writeFile('node_description.txt', content, (err) => {
  if (err) {
    throw err;
  }
  console.log('Fajl sa novim sadrzajem je kreiran.');
});
```

Funkcija `writeFile` kao prvi argument očekuje ime fajla nad kojim se vrši upis, zatim sadržaj koji se upisuje i funkciju sa povratnim pozivom koju treba izvršiti posle upisa. Opciono, nakon sadržaja koji je tekstualnog tipa se može navesti kodna shema sadržaja. To je podrazumevano UTF-8 kodna šema.

#### Modul url

Modul `url` omogućava parsiranje i analiziranje URL adresa. Sledeći primer ilustruje kako pomoću odgovarajućih svojstava možemo izdvojiti mrežno ime, port, putanju i parametre pretrage za zadatu adresu.

```javascript
const url = require('url');

const testURL = new URL(
  'http://testwebsite.com:3000/users.html?id=1000&status=active'
);

// string reprezentacija URL-a
console.log(testURL.href);

// mrezno ime
console.log(testURL.hostname);

// port
console.log(testURL.port);

// putanja do trazenog fajla
console.log(testURL.pathname);

// parametri pretrage u tekstualnoj formi
console.log(testURL.search);

// parametri pretrage u formi niza
console.log(testURL.searchParams);
testURL.searchParams.forEach((name, value) => {
  console.log(name, value);
});
```

Primetimo da parametre pretrage možemo izdvojiti u formi stringa, svojstvom `search`, ili u formi niza, svojstvom `searchParams`. Drugi pristup omogućava lakšu manipulaciju, a na nivou modula postoje i funkcije za efikasno pretraživanje niza parametara po ključevima i vrednostima i njegovo dopunjavanje, menjanje i brisanje.

#### Modul events

Zbog svoje arhitekture i programiranja zasnovanog na događajima, ovaj modul igra važnu ulogu u razvoju Node.js aplikacija. On omogućava kreiranje objekata emitera (engl. emitters) koji imaju mogućnost emitovanja događaja (engl. events). Glavne karakterizacije događaja su imena i podaci. Ove događaje dalje mogu da prate objekti osluškivači (engl. listeners) i da preduzimaju željene radnje obično opisane funkcijama nad podacima koje emiteri generišu.

![Petlja događaja](assets/event_loop.jpg)

Sledeći primer ilustruje kreiranje emitera i osluškivača.

```javascript
const EventEmitter = require('events');

class SweetEmitter extends EventEmitter {}
const sweetEmitter = new SweetEmitter();

sweetEmitter.on('chocolateEvent', (data) => {
  console.log('With joy we serve: ', data);
});

sweetEmitter.emit('chocolateEvent', 'cake');
sweetEmitter.emit('chocolateEvent', 'ice cream');
sweetEmitter.emit('chocolateEvent', 'pudding');
```

Emiteri se kreiraju proširivanjem klase `EventEmitter` i predstavljaju njene instance. Oni funkcijom `emit` dalje objavljuju imenovane događaje uz podatke koji ih opciono prate. Funkcijom `on` se vrši registrovanje osluškivača za događaje emitera. U našem slučaju kreiran je emiter sa imenom sweetEmitter koji emituje događaje sa imenom 'chocolateEvent' koje prate imena slatkiša. Funkcija osluškivač koja se izvršava ispisuje imena ovih slatkiša na standardni izlaz.

U opštem slučaju podaci koji emiteri emituju mogu biti i objekti i nizovi vrednosti. U tom slučaju, potrebno je usaglasiti potpise funkcija osluškivača. Sledeći primer ilustruje sabiranje brojeva pomoću emitera i osluškivača.

```javascript
const EventEmitter = require('events');

class MathEmitter extends EventEmitter {}
const mathEmitter = new MathEmitter();

mathEmitter.on('sum', (a, b) => {
  console.log(`${a}+${b}=${a + b}`);
});

mathEmitter.emit('sum', 15, 5);
mathEmitter.emit('sum', 10, 7);
```

Napomenuli smo da se funkcijom `on` vrši registrovanje osluškivača za navedeni imenovani događaj. Nakon što se događaj desi, svi registrovani osluškivači se izvršavaju sinhrono i to redosledom kojim su i navedeni. Motivacija za ovakvo ponašanje je ista kao i u slučaju `RxJS` biblioteke tj. treba da predupredi logičke greške i utrkivanje događaja. Ukoliko je ipak potrebno anishrono ponašanje ovakvih funkcija, može se navesti omotač sa imenom `setImmediate` oko tela funkcije. Sledeći primer ilustruje takvo ponašanje.

```javascript
const EventEmitter = require('events');

// kreiranje emitera
class MathEmitter extends EventEmitter {}
const mathEmitter = new MathEmitter();

mathEmitter.on('sum', (a, b) => {
  setImmediate(() => {
    console.log(`${a}+${b}=${a + b}`);
  });
});

mathEmitter.emit('sum', 15, 5);
mathEmitter.emit('sum', 10, 7);
```

Zbog asinhrone prirode registrovanih funkcija, mogući su ispisi 20, 17 i 17, 20.

Ukoliko je potrebno na neki događaj odreagovati najviše jednom, umesto funkcije `on` za prijavljivanje se može iskoristiti funkcija `once`.

```javascript
const EventEmitter = require('events');

class SweetEmitter extends EventEmitter {}
const sweetEmitter = new SweetEmitter();

sweetEmitter.once('chocolateEvent', (data) => {
  console.log('With joy we serve: ', data);
});

sweetEmitter.emit('chocolateEvent', 'cake');
sweetEmitter.emit('chocolateEvent', 'ice cream');
sweetEmitter.emit('chocolateEvent', 'pudding');
```

Sada će biti ispisana samo poruka "With joy we serve: cake".

Brisanje osluškivača moguće je funkcijama `removeListener` ili `removeAllListener`. Ako u prethodnom primeru funkciju sa povratnim pozivom zamenimo ekvivalentnom imenovanom funkcijom `printMessage` i obrišemo osluškivač pozivom `removeListener('chocolateEvent', printMessage)` biće ispisane samo poruke "With joy we serve: cake" i "With joy we serve: ice cream".

```javascript
const EventEmitter = require('events');

class SweetEmitter extends EventEmitter {}
const sweetEmitter = new SweetEmitter();

const printMessage = (data) => {
  console.log('With joy we serve: ', data);
};

sweetEmitter.on('chocolateEvent', printMessage);

sweetEmitter.emit('chocolateEvent', 'cake');
sweetEmitter.emit('chocolateEvent', 'ice cream');

sweetEmitter.removeListener('chocolateEvent', printMessage);

sweetEmitter.emit('chocolateEvent', 'pudding');
```

Ukoliko je osluškivače potrebno samo privremeno suspendovati može se koristiti funkcija `off`.

#### HTTP

Modul `http` omogućava obradu i generisanje HTTP zahteva. Kako je primarna funkcija veb servera prijem HTTP zahteva, njihova obrada, pokretanje očekivanih radnji i generisanje rezultata u formi HTTP odgovora, kroz funkcionalnosti ovog modula proći ćemo praktično baš implementacijom jednog Node.js veb servera. Ove funkcionalnosti ćemo u budućnosti zameniti radnim okvirom `express.js` koji će nam dati više slobode i fleksibilnosti u radu. Ipak, za samo razumevanje celog procesa dobro je da budemo upoznati sa implementacionim detaljima.

Veb server koji ćemo implementirati će biti u stanju da na zahtev klijenta isporuči stranice index.html i about.html. Podsetimo se da se i za svaki resurs koji se koristi u kreiranju veb stranica tipa lokalnih CSS stilova, slika ili JavaScript fajlova generiše po jedan HTTP zahtev ka serveru. Stoga ćemo podržati i isporučivanje ovakvih vrsta fajlova. U slučaju kada se zahteva stranica koja ne postoji, prusmerićemo korisnika na poznatu 404.html stranicu.

Za kreiranje veb servera možemo iskoristiti funkciju `createServer`. Njen argument je funkcija sa povratnim pozivom nad argumentima koji predstavljaju pristigli HTTP zahtev i delimično konfigurisani HTTP odgovor. Po pravilu serveri su javni tj. operišu na poznatim adresama i portovima. Naš server će biti dostupan u lokalu na adresi `localhost` i portu `5000.` Ovo podešavanje će nam omogućiti funkcija `listen` kreiranog veb servera čijim izvršavanjem se server i aktivira. Svaki put kada HTTP zahtev stigne do veb servera, izvršiće se funkcija sa povratnim pozivom zadata kao argument `createServer` funkcije.

```javascript
const http = require('http');

const server = http.createServer((req, res) => {
  // posao koji server treba da obavi
});

server.listen(5000, () => {
  console.log('Server je pokrenut!');
});
```

Ukoliko prethodni kod sačuvamo u datoteci `server.js`, izvršavanje komande `nodemon server.js` omogući će nam da pratimo dalje aktivnosti u pregledaču na adresi `http://localhost:5000/`.

HTTP odgovor predstavlja se klasom `ServerResponse`. Metode i svojstva na nivou ove klase omogućavaju podešavanje statusa odgovora, konfigurisanje polja zaglavlja i navođenja samog tela odgovora. Vrednosti statusnog koda i sama zaglavlja treba da budu usaglašena sa HTTP protokolom, dok telo odgovora treba da bude na nivo tehnologija koje pregledači mogu da prikažu. Sledeći fragment koda postavlja u odgovoru statusni kod `200`, zaglavlje `Content-Type: text/html` i telo odgovora na `<h1> Hello from Node.js server! </h1>`.

```javascript
res.writeHead(200, {
  'Content-Type': 'text/html',
});
const body = '<h1> Hello from Node.js server!</h1>';
res.write(body);
res.end();
```

Funkcija `writeHead` očekuje statusni kod i objekat sa vrednostima polja zaglavlja. Isti zadatak se mogao postići kombinovanjem sledećih naredbi kojima se podešavaju pojedinačne vrednosti.

```javascript
res.status = 200;
res.setHeader('Content-Type', 'text/html');
```

Funkcijom `write` se ispisuje telo odgovora. Ispisivanje se može uraditi jednom ili kroz nekoliko manjih poziva ove funkcije. Sadržaji koji se ispisuju treba birati i formatirati tako da ih klijenti mogu interpretirati. To će za nas najčešće biti tekstualni sadržaji u HTML ili JSON formatu ili binarne reprezentacije multimedija. Podrazumevana kodna šema sadržaja je UTF-8 ali se može promeniti navođenjem opcionog `encoding` parametra.

Funkcijom `end` se završava slanje odgovora. Opciono joj se kao argument može zadati funkcija sa povratnim pozivom koju treba izvrišiti nakon slanja odgovora. Treba imati na umu da ukoliko se ovaj poziv izostavi, odgovor neće biti poslat.

Uobičajeno je da se HTML stranice koje se isporučuju klijentima čuvaju u `public` direktorijumu veb servera. U ovom direktorijumu se mogu naći i poddirektorijumi sa imenima `assets`, `images`, `css` ili `js` u kojima se semantički grupišu resursi potrebni za kreiranje veb aplikacije. Sledećim kodom se dohvata stranica sa imenom `index.html` direktorijuma `public` i isporučuje korisniku.

```javascript
const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) => {
  const indexPath = path.join(__dirname, 'public', 'index.html');
  fs.readFile(indexPath, (err, content) => {
    if (err) {
      throw err;
    }
    res.writeHead(200, {
      'Content-Type': 'text/html',
    });
    res.write(content);
    res.end();
  });
});

server.listen(5000, () => {
  console.log('Server je pokrenut!');
});
```

Ovaj kod kombinuje znanja vezana za rad sa fajl sistemom i samo generisanje HTTP odgovora. Na osnovu putanje tekućeg direktorijuma evaluiramo putanju zahtevanog fajla, a zitim u funkciji sa povratnim pozivom koja odgovara čitanju sadržaja fajla navodimo fragment koda kojim se generiše HTTP odgovor. Sam sadržaj index.html stranice je rudimentaran i predstavlja minimalni osnovni kontekst.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Home page</title>
  </head>
  <body>
    <h1>Hello from Node.js server!</h1>
  </body>
</html>
```

Priloženi kod dalje možemo doterati dodavanjem fragmenta koda za obradu greške u slučaju neuspešnog čitanja fajla. Praksa je da sve greške koje nastanu na serveru propratimo odgovorima sa statusnim kodovima čije su vrednosti od 500 pa naviše. Tako umesto ispaljivanja izuzetka, možemo generisati HTTP odgovor sa kodom 500 i odgovarajućom statusnom porukom.

```javascript
fs.readFile(indexPath, (err, content) => {
  if (err) {
    res.writeHead(500, 'Server error!');
    res.end();
  } else {
    res.writeHead(200, {
      'Content-Type': 'text/html',
    });
    res.write(content);
    res.end();
  }
});
```

Dodatu funkcionalnost servera možemo testirati navođenjem putanje pogrešnog fajla ili privremenim uklanjanjem datoteke index.html iz public direktorijuma. U jezičku *Networks* veb alata pregledača, u sekciji *Headers* bi trebali da vidimo kod 500 i pridruženu poruku obično ispisanu crvenom bojom.

Primer koji smo upravo videli predstavlja statičko isporučivanje sadržaja jer je stranica u formi u kojoj smo je poslali klijentu već postojala. Kroz radni okvir `express.js` naučićmo kako da dinamički kreiramo i isporučujemo sadržaje.

Jasno nam je da na ovaj način možemo isporučiti bilo koju stranicu klijentu. Ostaje nam da saznamo koju tačno stranicu klijent želi da učita i kako možemo da prilagodimo zaglavlja odgovora zahtevanim sadržajima. To ćemo postići analizom HTTP zahteva klijenata.

HTTP zahtev klijenta se u Node.js okruženju predstavlja klasom `ClientRequest`. Svojstva i metode ove klase omogućavaju očitavanje podataka zahteva. Tako se, na primer, iz zahtev `req` koji stiže do veb servera svojstvom `method` može pročitati metod (najčešće GET, PUT, POST i DELETE), a preko svojstva `headers` vrednosti polja zaglavlja. Često se očitava polje `user-agent` na osnovu kojeg se za različite operativne sisteme i korisničke agente isporučuju optimizovani sadržaji. 

```javascript
const method = req.method;
const userAgent = req.headers['user-agent'];
```

Putanja zahtevanog fajla se može očitati preko svojstva `url`. Na primer, za zahtev `http://localhost:5000/public/css/style.css` ovo svojstvo će imati vrednost `/public/css/style.css` tj. biće uzet u obzir ceo sadržaj nakon mrežnog imena i porta.

U kodu koji sledi se u zavisnosti od zahtevanog fajla pripremaju putanje stranica za isporučivanje i propratne informacije o tipu.

```javascript
let contentFile;
let contentType;

contentFile = req.url;
if (req.url == '/') {
  contentFile = 'index.html';
}

const fileExtension = path.extname(contentFile);

switch (fileExtension) {
  case '.js':
    contentType = 'text/javascript';
    break;
  case '.html':
    contnetType = 'text/html';
    break;
  case '.json':
    contentType = 'application/json';
    break;
  case '.css':
    contentType = 'text/css';
    break;
  case '.jpg':
    contentType = 'image/jpg';
    break;
  default:
    contentType = 'text/html';
}

const contentPath = path.join(__dirname, 'public', contentFile);
```

Prvo proveravamo da li je putanja zahtevanog fajla '/' kako bi u oba slučaja zadavanja adresa `http://localhost:5000/` i `http://localhost:5000/index.html` isporučili sadržaj index.html stranice. Potom analiziramo ekstenziju zahtevanog fajla i u skladu sa njom pripremamo vrednost `Content-Type` odgovora.

Ostalo nam je još da dodamo proveru kojom bi u slučaju pristupa nepostojećim veb stranicama prikazali `404.html` stranicu. Tu proveru možemo dodati u delu sa očitavanjem sadržaja fajla.

```javascript
if (err) {
    if (err.code == 'ENOENT') {
        const path404 = path.join(__dirname, 'public', '404.html');
        fs.readFile(path404, (err, content) => {
            res.writeHead(200, {'
                Content-Type': 'text/html' 
            });
            res.write(content);
            res.end(); 
        });
    } else {
        res.writeHead(500, 'Server error!');
        res.end();
    }
} else {
    res.writeHead(200, {
      'Content-Type': contentType,
    });
    res.write(content);
    res.end();
}
```
Ukoliko zahtevana stranica fizički ne postoji, učitaće se sadržaj stranice 404.html i isporučiti kao odgovor. Prilikom provere ispitivali smo vrednost pomenutog `code` svojstva objekta greške. Ova implemetacija nije savršena, ali će nam mehanizam rutiranja `express.js` radnog okvira pomoći da je unapredimo.

Spajanjem svih delova u celinu, dobijamo prvu implementaciju Node.js veb server.

```javascript
const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) => {
  let contentFile;
  let contentType;

  contentFile = req.url;
  if (req.url == '/') {
    contentFile = '/index.html';
  }

  const fileExtension = path.extname(contentFile);
  switch (fileExtension) {
    case '.js':
      contentType = 'text/javascript';
      break;
    case '.html':
      contentType = 'text/html';
      break;
    case '.json':
      contentType = 'application/json';
      break;
    case '.css':
      contentType = 'text/css';
      break;
    case '.jpg':
      contentType = 'image/jpg';
      break;
    default:
      contentType = 'text/html';
  }

  const contentPath = path.join(__dirname, 'public', contentFile);

  fs.readFile(contentPath, (err, content) => {
    if (err) {
      if (err.code == 'ENOENT') {
        const path404 = path.join(__dirname, 'public', '404.html');
        fs.readFile(path404, (err, content404) => {
          res.writeHead(200, {
            'Content-Type': 'text/html',
          });
          res.write(content404);
          res.end();
        });
      } else {
        res.writeHead(500, 'Server error!');
        res.end();
      }
    } else {
      res.writeHead(200, {
        'Content-Type': contentType,
      });
      res.write(content);
      res.end();
    }
  });
});

server.listen(5000, () => {
  console.log('Server je pokrenut!');
});
```

U dosadašnjoj priči ulogu veb klijenta je igrao pregledač. Zadavanjem odgovarajućih veb adresa tj. URL-ova generisani su HTTP zahtevi od strane pregledača ka serveru. U okruženju Node.js moguće je implementirati i veb klijente. Sledeći primer ilustruje kako se to može uraditi. 

```javascript
const http = require('http');

const options = {
    hostname: "http://localhost:5000",
    path: "/about.html",
    method: "GET",
    headers: {Accept: "text/html"}
};
const client = http.request(options, response => {
    console.log("Statusni kod odgovora servera: ", response.statusCode);
});

client.on('error', error =>{
    console.log('Greska: ', error);
});

client.end();
```
Preko objekta `options` zadaju se parametri zahteva: mrežno ime servera, putanja traženog fajla, metod i odgovarajuća polja zaglavlja. Funkcijom `request` se šalje zahtev serveru. Drugi argument ove funkcije je funkcija sa povratnim pozivom koja će se izvršiti nakon što odgovor sa servera pristigne.  Rezultat funkcije je tok podataka koji ostaje otvoren sve dok se esplicitno ne pozove funkcija `end`. Ovaj tok podataka mora da osluškuje moguće greške tj. događaje sa imenom `error` pa je zbog toga pozivom funkcije `on` ovo i realizovano. Ukoliko se ovaj fragment izostavi, prijavljivaće se greška "Unhelded 'error' event".  


### Literatura za ovaj deo:
- [zvanična Node.js dokumentacija](https://nodejs.org/docs/latest-v13.x/api/)
- [tutorial Node.js zajednice](https://nodejs.dev/)
- [Eloquent JavaScript](https://eloquentjavascript.net/20_node.html)